using Test

#======  Generate data with known underlying parameters and statistics  =======# 
p = [100.0, 4.5, 0.33]
gamma_model = simulate(GammaModel(), ImpulseInput(), p)
t = range(0.0, stop = 100, length = 21)
y = gamma_model.(t)


#======================  Test the statistics estimators  ======================# 
@test isapprox(estimate_mean(PDF(), t, y), p[2] / p[3]; rtol = 1e-2)
@test isapprox(estimate_mean(CDF(), pdf2cdf(t, y)...), p[2] / p[3]; rtol = 1e-2)

@test isapprox(estimate_variance(PDF(), t, y), p[2] / p[3]^2; rtol = 1e-1)
@test isapprox(estimate_variance(CDF(), pdf2cdf(t, y)...), p[2] / p[3]^2; rtol = 1e-1)

#======================  Test the parameter estimation  =======================# 
@test all(isapprox.(estimate_param(PDF(), t, y), p; rtol = 1e-1))
@test all(isapprox.(estimate_param(CDF(), pdf2cdf(t, y)...), p; rtol = 1e-1))
