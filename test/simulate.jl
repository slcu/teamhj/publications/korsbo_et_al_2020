using Test 

inputs = [DecayingInput(), ImpulseInput(), RampInput(), StepInput(), NoiseInput(), WaveInput()]

params = Dict(
              FixedStepModel() => [2., 0.1, 0.1, 0.1],
              FixedRateModel() => [2., 3, 0.1],
              DDEModel() => [2., 3., 0.1],
              GammaDDEModel() => [2., 3., 0.1],
             )

model = FixedStepModel()
input = ImpulseInput()
input = DecayingInput()
#========================  Test the scaling parameter  ========================# 

for input in inputs[1:end-1]
    for model in keys(params)
        @show model, input
        γ1 = 2.
        γ2 = 4.
        sol1 = simulate(model, input, vcat(γ1, params[model][2:end]))
        if input isa NoiseInput 
            input = RepeatedNoiseInput(sol1)
            sol1 = simulate(model, input, vcat(γ1, params[model][2:end]))
        end
        sol2 = simulate(model, input, vcat(γ2, params[model][2:end]))
        t_range = range(0., stop=100., length=100)
        if hasfield(typeof(sol1), :t)
            @test all([isapprox(sol1(t; idxs=2), (γ1/γ2)*sol2(t; idxs=2), rtol=1e-1) for t in t_range])
            @test !all(isapprox.(sol1.(t; idxs=2), 0))
        else
            @test all([isapprox(sol1(t), (γ1/γ2)*sol2(t), rtol=1e-3) for t in t_range])
            @test !all(isapprox.(sol1.(t), 0))
        end
    end
end

#==============================================================================#
#============  Fixed-rate and fixed-step equivalency for r_i = r  =============# 
#==============================================================================#

for input in inputs
    p = [100. * rand(), 10., 0.1]
    tstop = 500.
    sol1 = simulate(FixedStepModel(), input, vcat(p[1], fill(p[3], round(Int, p[2]))); tstop=tstop)
    input isa NoiseInput && (input = RepeatedNoiseInput(sol1))
    sol2 = simulate(FixedRateModel(), input, p; tstop=tstop)
    t_range = range(50., stop=150., length=1000)
    if input isa ImpulseInput
        @show typeof(input)
        @test all([isapprox(sol1(t; idxs=2), sol2(t), rtol=1e-3) for t in t_range])
    else
        @show typeof(input)
        @test all([isapprox(sol1(t; idxs=2), sol2(t; idxs=2), rtol=1e-3) for t in t_range])
    end
end

#==============================================================================#
#=====  Impulse and decaying input equivalency for the right parameters  ======# 
#==============================================================================#

p = vcat(100. * rand(), rand(10))
tstop = 500.
sol1 = simulate(FixedStepModel(), ImpulseInput(), p)
sol2 = simulate(FixedStepModel(), DecayingInput(p[2], p[2]), vcat(p[1], p[3:end]))
t_range = range(10., stop=100., length=1000)
@test all([isapprox(sol1(t; idxs=2), sol2(t; idxs=2), rtol=1e-3) for t in t_range])


p = vcat(100. * rand(), 10, 0.1)
tstop = 500.
sol1 = simulate(FixedRateModel(), ImpulseInput(), p)
sol2 = simulate(FixedRateModel(), DecayingInput(p[3], p[3]), [p[1], p[2]-1, p[3]])
t_range = range(10., stop=100., length=1000)
@test all([isapprox(sol1(t), sol2(t; idxs=2), rtol=1e-3) for t in t_range])

#==============================================================================#
#==============  Gamma and fixed-rate equivalency for integer n  ==============# 
#==============================================================================#

sol_gamma = simulate(GammaModel(), ImpulseInput(), [1., 5., 0.2])
sol_rate =  simulate(FixedRateModel(), ImpulseInput(), [1., 5., 0.2])
for t in 1:100
    @test sol_gamma(t) ≈ sol_rate(t)
end

if false
plot(sol1; label="1", vars=2)
plot!(sol2, label="2", vars=2)

plot(sol1, 0.:0.1:200)
plot!(sol2, label="2", vars=2)
end
