################################################################################
#########################  Define the parameter space  #########################
################################################################################
"""
    transform_param(model, params)

Transform the optimiser search-space from U(0,1) to something more appropriate.
"""
function transform_param end
#= @inline transform_param(::FixedStepModel, p) = @. 10. ^ (6. * p - 3.) =#
#= @inline transform_param(::FixedRateModel, p) = [10. .^ (2. * p[1] - 1), ceil(30*p[2]), 10. ^ (4. * p[3] - 2.)] =#
#= @inline transform_param(::AbstractFunctionalModel, p) = [10. ^ (6*p[1] - 4), 30. * p[2] + 1, 10. ^ (6*p[3] - 3)] =#
#= @inline transform_param(::GammaDDEModel, p) = [10. ^ (6*p[1] - 4), 30. * p[2] + 3, 10. ^ (6*p[3] - 3)] =#
#= @inline transform_param(::DDEModel, p) = [10. ^ (6*p[1] - 4), 10. .^ (6 * p[2] - 2), 10. ^ (5*p[3] - 4)] =#

@inline transform_param(::FixedStepModel, p) = vcat(10. ^ (8 * p[1] - 4), 10. .^ (8. .* p[2:end] .- 4.))
@inline transform_param(::FixedRateModel, p) = [10. .^ (8. * p[1] - 4), ceil(30*p[2]), 10. ^ (8. * p[3] - 4.)]
@inline transform_param(::AbstractFunctionalModel, p) = [10. ^ (8*p[1] - 4), 29. * p[2] + 1, 10. ^ (8*p[3] - 4)]
@inline transform_param(::GammaHillModel, p) = [10. ^ (8*p[1] - 4), 29. * p[2] + 1, 10. ^ (8*p[3] - 4), 10. ^(8*p[4] - 4), 10*p[end]]
@inline transform_param(::GammaDDEModel, p) = [10. ^ (8*p[1] - 4), 27. * p[2] + 3, 10. ^ (8*p[3] - 4)]
@inline transform_param(::DDEModel, p) = [10. ^ (8*p[1] - 4), 10. .^ (8 * p[2] - 4), 10. ^ (6*p[3] - 3)]
@inline transform_param(::FixedRateMMModel, p) = [10. .^ (8. * p[1] - 4), ceil(30*p[2]), 10. ^ (8. * p[3] - 4.), 10. ^ (8 * p[4] - 7)]
@inline transform_param(::FixedRateHillModel, p) = [10. .^ (8. * p[1] - 4), ceil(30*p[2]), 10. ^ (8. * p[3] - 4.), 10. ^ (8 * p[4] - 7), 10*p[5]]
# @inline transform_param(::FixedRateCascadeModel, p) = [10. .^ (5. * p[1] - 4), ceil(30*p[2]), 10. ^ (2. * p[3] - 1.), 10. ^ (6 * p[4] - 3)]
@inline transform_param(::FixedRateCascadeModel, p) = [10. .^ (7. * p[1] - 6), ceil(20*p[2]), 10. ^ (4. * p[3] - 2.), 10. ^ (6 * p[4] - 3)]
function transform_param(::MMChainModel, p) 
    p_new = similar(p)
    # p_new[1:3:end] .= 10. .^ (11 .* p[1:3:end] .- 3)
    # p_new[2:3:end] .= 10. .^ (11 .* p[2:3:end] .- 3)
    # p_new[3:3:end] .= 10. .^ (11 .* p[3:3:end] .- 3)
    p_new[1] = 10. ^ (6*p[1]-4)
    p_new[2:2:end] .= 10. .^ (6 .* p[2:2:end] .- 4)
    p_new[3:2:end] .= 10. .^ (11 .* p[2:2:end] .- 8)
    return p_new
end
function transform_param(::CascadeModel, p) 
    p_new = similar(p)
        # p_new[1:3:end] .= 10. .^ (11 .* p[1:3:end] .- 3)
        # p_new[2:3:end] .= 10. .^ (11 .* p[2:3:end] .- 3)
        # p_new[3:3:end] .= 10. .^ (11 .* p[3:3:end] .- 3)
    # p_new[1:3:end] .= 10. .^ (11 .* p[1:3:end] .- 3)
    # p_new[2:3:end] .= 10. .^ (11 .* p[2:3:end] .- 3)
    # p_new[3:3:end] .= 10. .^ (11 .* p[3:3:end] .- 3)
    p_new[1:3:end] .= 10. .^ (13 .* p[1:3:end] .- 5)
    p_new[2:3:end] .= 10. .^ (13 .* p[2:3:end] .- 5)
    p_new[3:3:end] .= 10. .^ (13 .* p[3:3:end] .- 5)
    return p_new
end

"""
    transform_data_param(model, input, params)

Transfom the distribution of data-generating reaction rates from U(0,1) to a 
log-uniform distribution.
"""
function transform_data_param end
@inline transform_data_param(::FixedStepModel, ::AbstractInput, p) = vcat(1., 10. .^ (3. .* p[2:end] .- 2.))
@inline transform_data_param(::FixedStepModel, ::AbstractNoiseInput, p) = vcat(1., 10. .^ (2. .* p[2:end] .- 1.))
@inline transform_data_param(::FixedStepModel, ::WaveInput, p) = vcat(1., 10. .^ (1. .* p[2:end] .- 0.))
#= @inline transform_data_param(::CascadeModel, ::AbstractInput, p) = @. 10. ^ (2. * p - 1.) =#
function transform_data_param(::CascadeModel, ::AbstractInput, p) 
    p_new = similar(p)
    # p_new[1:3:end] .= 10. .^ (2 .* p[1:3:end] .- 0.5)
    # p_new[2:3:end] .= 10. .^ (2 .* p[2:3:end] .- 1)
    # p_new[3:3:end] .= 10. .^ (2 .* p[3:3:end] .- 1)
    p_new[1:3:end] .= 10. .^ (3 .* p[1:3:end] .- 1)
    p_new[2:3:end] .= 10. .^ (3 .* p[2:3:end] .- 1)
    p_new[3:3:end] .= 10. .^ (2 .* p[3:3:end] .- 1)
    return p_new
end

function transform_data_param(::MMChainModel, ::AbstractInput, p) 
    p_new = similar(p)
    p_new[1] = 1.
    p_new[2:2:end] .= 10. .^ (3 .* p[2:2:end] .- 2)
    p_new[3:2:end] .= 10. .^ (3 .* p[3:2:end] .- 2)
    return p_new
end


default_tstop(::AbstractInput) = 5000
default_tstop(::RampInput) = 500
default_tstop(::WaveInput) = 30
default_tstop(::AbstractNoiseInput) = 200

################################################################################
########################  Optimisation function calls  #########################
################################################################################

"""
    opt_single(model, input, x_data, y_data, nr_params, tstop)

Run an optimisation of a single model towards the data. 
This is mainly for internal use. 
"""
function opt_single(model::AbstractModel, input::AbstractInput, x_data, y_data, nr_params, tstop; silent=false, cost_function = normalized_integral_cost, max_steps = Int(nr_params*4e3), kwargs...)
    silent || println("Optimising $model")
    res = bboptimize(
        p -> cost_function(model, input, transform_param(model, p), x_data, y_data, tstop; kwargs...);
        SearchRange=(0., 1.),
        Method=:adaptive_de_rand_1_bin_radiuslimited,
        # Method=:de_rand_1_bin,
        NumDimensions=nr_params,
        MaxSteps=max_steps,
		# MinDeltaFitnessTolerance = 0,
        TraceInterval=5,
        TraceMode= silent ? :silent : :compact
    )
    p_opt = transform_param(model, best_candidate(res))
    cost = best_fitness(res)
    return (p_opt, cost)
end

"""
    opt(input; min_nodes=1, max_nodes=50)

optimise a full set of models.

1.  generate a synthetic data set of a random pathway length.
2.  optimise the fixed rate model and fixed step models of lengths 1 to 5.
3.  return a Result object which contains the true synthetic parameters,
    the optimised parameters and their associated cost.
"""
function opt(input::AbstractInput; min_nodes=1, max_nodes = 50, kwargs...)
    nr_data_nodes = rand(min_nodes:max_nodes)
	data_param = transform_data_param(FixedStepModel(), input, rand(nr_data_nodes+1)) 
    opt(input, data_param; kwargs...)
end

function opt(input::NoiseInput, data_param::Vector; tstop=default_tstop(input), kwargs...) 
    target = simulate(
					  FixedStepModel(), 
					  input, 
					  data_param; 
					  tstop=tstop, 
					  )
	opt(RepeatedNoiseInput(target), data_param; tstop=tstop, kwargs...)
end

generate_target(p::AbstractParameter; kwargs...) = generate_target(p.model, p.input, p.param, p.tstop; kwargs...)

function generate_target(model::AbstractModel, input::AbstractInput, data_param, tstop; kwargs...)
    sim = simulate(model, input, data_param; tstop=tstop, kwargs...)

    success = true

    ### check for indications that the target data generation resulted in an inappropriate target
    # Did the simulation succeed?
    sim.retcode in [:Success, :Terminated] || (success = false)

    # Did the saturation plateau end before the end of the simulation?
    y_max = get_y_max(model, data_param) 
    y_max < 1e-4 && (success = false)

    # Is the last value of the last step relatively close to zero?
    last_value = sim.u[end][end] 
    - y_max / 1e3 < last_value < y_max / 10 || (success = false)

    return (sim.t, hcat(sim.u...)[end, :], sim.t[end], success)
end

function generate_target(model::AbstractModel, input::RepeatedNoiseInput, data_param, tstop; kwargs...)
    sim = simulate(model, input, data_param; tstop=tstop, kwargs...)
    x_targets = sort(vcat(input.positive_jump_times, input.negative_jump_times))
    y_targets = sim(x_targets; idxs=2)

    success = true

    ### check for indications that the target data generation resulted in an inappropriate target
    # Did the simulation succeed?
    sim.retcode in [:Success, :Terminated] || (success = false)
    return (x_targets, y_targets, sim.t[end], success)
end

function generate_target(model::AbstractModel, input::Union{ImpulseInput, DecayingInput, StepInput}, data_param, tstop; kwargs...)
    sim = simulate(model, input, data_param; tstop=tstop, callback = TerminateSteadyState( 1e-8, 1e-6), kwargs...)

    success = true
    ### check for indications that the target data generation resulted in an inappropriate target
    # Did the simulation succeed?
    sim.retcode in [:Success, :Terminated] || (success = false)
    return (sim.t, hcat(sim.u...)[end, :], sim.t[end], success)
end

function generate_target(model::AbstractModel, input::PiecewiseInput, data_param, tstop; kwargs...)
    target = simulate(model, input, data_param; tstop=tstop, callback = TerminateSteadyState( 1e-8, 1e-6), kwargs...)
    if target.t[end] < 100
        target = simulate(model, input, data_param; tstop=200, kwargs...)
    end
    success = true
    ### check for indications that the target data generation resulted in an inappropriate target
    # Did the simulation succeed?
    sim.retcode in [:Success, :Terminated] || (success = false)
    return (target.t, hcat(target.u...)[end, :], target.t[end], success)
end

function opt(input::AbstractInput, data_param::Vector; tstop=default_tstop(input), kwargs...) 
    x_data, y_data, tstop, success = generate_target(FixedStepModel(), input, data_param, tstop; kwargs...)
    opt(input, data_param, x_data, y_data; tstop=tstop, kwargs...)
end

# function opt(input::AbstractInput, data_param::Vector; tstop=default_tstop(input), silent = true, kwargs...) 

const opt_models = Dict(
                    # 1 => (FixedStepModel(), 2),
                    # 2 => (FixedStepModel(), 3),
                    # 3 => (FixedStepModel(), 4),
                    # 4 => (FixedStepModel(), 5),
                    # 5 => (FixedStepModel(), 6),
                    :fixed_rate => (FixedRateModel(), 3),
                    :one_node => (FixedStepModel(), 2),
                    :two_node => (FixedStepModel(), 3),
                    :three_node => (FixedStepModel(), 4),
                    :four_node => (FixedStepModel(), 5),
                    :five_node => (FixedStepModel(), 6),
                    :fixed_rate => (FixedRateModel(), 3),
                    :dde => (DDEModel(), 3),
                    :gamma => (GammaGeneralModel(), 3),
                    :cascade_1 => (CascadeModel(), 3*1),
                    :cascade_2 => (CascadeModel(), 3*2),
                    :cascade_3 => (CascadeModel(), 3*3),
                    :mm_1 => (MMChainModel(), 2*1+1),
                    :mm_2 => (MMChainModel(), 2*2+1),
                    :mm_3 => (MMChainModel(), 2*3+1),
                    :fixed_rate_hill => (FixedRateHillModel(), 5),
                    :fixed_rate_mm => (FixedRateMMModel(), 4),
                    :fixed_rate_cascade => (FixedRateCascadeModel(), 4),
                    )

function optimise(p::Result; model_keys = [], overwrite = false, kwargs...)
    @assert length(p.inputs) == 1 "optimise is only designed for a single input. The supplied parameter set has multiple."
    overwrite || filter!(x -> !(x in keys(p.fitted_models)), model_keys) 
    isempty(model_keys) && return p
    p_new = optimise(p.model, p.inputs[1], p.param; model_keys=model_keys, kwargs...)
    return Result(p.model, p.param, p.inputs, merge(p.fitted_models, p_new.fitted_models), p.tstop)
end

nparams(::CascadeModel, nsteps) = 3 * nsteps
nparams(::FixedStepModel, nsteps) = nsteps
nparams(::MMChainModel, nsteps) = 2 * nsteps + 1

function optimise(data_model::AbstractModel, input::AbstractInput; min_nodes=1, max_nodes=50, recursion_counter=0, tstop=default_tstop(input), kwargs...)
    nr_data_nodes = rand(min_nodes:max_nodes)
    data_param = transform_data_param(data_model, input, rand(nparams(data_model, nr_data_nodes)))

    x_data, y_data, tstop, success = generate_target(data_model, input, data_param, tstop; kwargs...)
    if success 
        return optimise(data_model, input, data_param, x_data, y_data; tstop=tstop, kwargs...)
    else
        @warn "The generated data had some issue so a new data set was generated."
        recursion_counter > 1e2 && error("The data generation was repeatedly unsuccessful.")
        return optimise(data_model, input; recursion_counter = recursion_counter + 1, min_nodes=min_nodes, max_nodes=max_nodes, kwargs...)
    end
end

function optimise(data_model::AbstractModel, input::AbstractInput, data_param; tstop=default_tstop(input), kwargs...)
    x_data, y_data, tstop, success = generate_target(data_model, input, data_param, tstop; kwargs...)
    success || @warn "The generated data had some issue"
    return optimise(data_model, input, data_param, x_data, y_data; tstop=tstop, kwargs...)
end

function optimise(data_model::AbstractModel, input::AbstractInput, data_param, x_data, y_data; tstop=default_tstop(input), model_keys=[], kwargs...)
    @assert !isempty(model_keys) 
    @assert all([key in keys(opt_models) for key in model_keys])
    results = Dict{Symbol, Tuple}()
    for model_key in model_keys
        model, nparam = opt_models[model_key]
        results[model_key] = (
                              opt_single(model, input, x_data, y_data, nparam, tstop; kwargs...)..., 
                              transform_param(model, zeros(nparam)),
                              transform_param(model, ones(nparam)),
                             )
    end
    return Result(data_model, data_param, [input], results, tstop)
end

#==============================================================================#
#============  Optimise function written for the linear analysis  =============# 
#==============================================================================#
#
# This is the one used a lot in the paper, but it is less flexible than the `optimise` 
# function.
#



function opt(input::AbstractInput, data_param::Vector, x_data::Vector, y_data::Vector; tstop=default_tstop(input), kwargs...) 
    gamma_model = input isa ImpulseInput ? GammaModel() : GammaGeneralModel()

	(p_gamma, c_gamma) = opt_single(gamma_model, input, x_data, y_data, 3, tstop; kwargs...)
    (p_h, c_h) = opt_single(FixedRateModel(), input, x_data, y_data, 3, tstop; kwargs...)
	(p_dde, c_dde) = opt_single(DDEModel(), input, x_data, y_data, 3, tstop; kwargs...)
	(p_1, c_1) = opt_single(FixedStepModel(), input, x_data, y_data, 2, tstop; kwargs...)
	(p_2, c_2) = opt_single(FixedStepModel(), input, x_data, y_data, 3, tstop; kwargs...)
	(p_3, c_3) = opt_single(FixedStepModel(), input, x_data, y_data, 4, tstop; kwargs...)
	(p_4, c_4) = opt_single(FixedStepModel(), input, x_data, y_data, 5, tstop; kwargs...)
	(p_5, c_5) = opt_single(FixedStepModel(), input, x_data, y_data, 6, tstop; kwargs...)

	p = ResultGeneral(
			   FixedStepModel(), 
			   data_param,
			   input,
			   p_1, 
			   c_1, 
			   p_2, 
			   c_2, 
			   p_3, 
			   c_3, 
			   p_4, 
			   c_4, 
			   p_5, 
			   c_5, 
			   FixedRateModel(), 
			   p_h, 
			   c_h, 
               gamma_model,
               p_gamma,
               c_gamma,
               DDEModel(),
               p_dde,
               c_dde,
			   tstop
			   )
    return p
end

function reoptimise(p::AbstractParameter, models::Vector; kwargs...)
    input = p.input
    tstop = p.tstop
    data_param = p.param
    gamma_model = p.gamma_model

    x_data, y_data, tstop, success = generate_target(FixedStepModel(), input, data_param, tstop; kwargs...)

    if :gamma in models
        (p_gamma, c_gamma) = opt_single(gamma_model, input, x_data, y_data, 3, tstop; kwargs...)
    else
        (p_gamma, c_gamma) = (p.gamma_param, p.gamma_cost)
    end
    if :fixed_rate in models
        (p_h, c_h) = opt_single(FixedRateModel(), input, x_data, y_data, 3, tstop; kwargs...)
    else
        (p_h, c_h) = (p.fixed_rate_param, p.fixed_rate_cost)
    end
    if :dde in models
        (p_dde, c_dde) = opt_single(DDEModel(), input, x_data, y_data, 3, tstop; kwargs...)
    else
        (p_dde, c_dde) = (p.dde_param, p.dde_cost)
    end
    if :one_step in models || :one_node in models || 1 in models
        (p_1, c_1) = opt_single(FixedStepModel(), input, x_data, y_data, 2, tstop; kwargs...)
    else
        (p_1, c_1) = (p.one_node_param, p.one_node_cost)
    end
    if :two_step in models || :two_node in models || 2 in models
        (p_2, c_2) = opt_single(FixedStepModel(), input, x_data, y_data, 3, tstop; kwargs...)
    else
        (p_2, c_2) = (p.two_node_param, p.two_node_cost)
    end
    if :three_step in models || :three_node in models || 3 in models
        (p_3, c_3) = opt_single(FixedStepModel(), input, x_data, y_data, 4, tstop; kwargs...)
    else
        (p_3, c_3) = (p.three_node_param, p.three_node_cost)
    end
    if :four_step in models || :four_node in models || 4 in models
        (p_4, c_4) = opt_single(FixedStepModel(), input, x_data, y_data, 5, tstop; kwargs...)
    else
        (p_4, c_4) = (p.four_node_param, p.four_node_cost)
    end
    if :five_step in models || :five_node in models || 5 in models
        (p_5, c_5) = opt_single(FixedStepModel(), input, x_data, y_data, 6, tstop; kwargs...)
    else
        (p_5, c_5) = (p.five_node_param, p.five_node_cost)
    end


	p_new = ResultGeneral(
			   FixedStepModel(), 
			   data_param,
			   input,
			   p_1, 
			   c_1, 
			   p_2, 
			   c_2, 
			   p_3, 
			   c_3, 
			   p_4, 
			   c_4, 
			   p_5, 
			   c_5, 
			   FixedRateModel(), 
			   p_h, 
			   c_h, 
               gamma_model,
               p_gamma,
               c_gamma,
               DDEModel(),
               p_dde,
               c_dde,
			   tstop
			   )
    return p_new
end

"""
    opt_homogeneous(; nr_data_nodes=5)

optimise the models towards data that is generated using a homogeneous parameter set.
"""
function opt_homogeneous(input::AbstractInput; nr_data_nodes=5, kwargs...)
    data_param = fill(1., nr_data_nodes + 1)
    opt(input, data_param; kwargs...)
end

struct HetroParam{T<:AbstractModel, T2<:AbstractInput} <: AbstractParameter{Float64}
    model::T
    param::Vector{Float64}
    cost::Float64
    input::T2
    n::Int
    δ::Float64
    data_param::Vector{Float64}
end

function opt_heterogeneous(model::AbstractModel; n=5, δ=3., free_param=3, tstop=default_tstop(ImpulseInput()), kwargs...)
    data_param = vcat(1., [n == 1 ? 1. : 10^(δ * (2 * i/(n-1) - 1)) for i in 0:(n-1)])
    
    input = ImpulseInput()
    x_data, y_data, tstop, success = generate_target(FixedStepModel(), input, data_param, tstop; kwargs...)

    (p, c) = opt_single(model, input, x_data, y_data, free_param, tstop; 
                                    kwargs...)

    return HetroParam(model, p, c, input, n, convert(Float64, δ), data_param)
    # opt(input, data_param; kwargs...)
end


#==============================================================================#
#===============  Optimise cascade model with multiple inputs  ================# 
#==============================================================================#
# This section is currently a bit less organised than I'd like, but the principle is simple.


function symmetrically_normalised_integral_loss(model::Function, data::Function; tstop=error("the kwarg tstop needs to be defined."), rtol=1e-3, kwargs...)
    # area_mismatch, err = quadgk(t->abs(model(t)-data(t)), 0, 1, tstop; rtol=rtol)
    # data_area, _, = quadgk(data, 0, 1, tstop; rtol=rtol)
    # model_area, _, = quadgk(model, 0, 1, tstop; rtol=rtol)
    nsamples = 1000
    trange = range(0., stop=tstop, length=nsamples)
    area_mismatch = sum(abs.(model.(trange) - data.(trange))) / nsamples * tstop
    normalisation = sum((max(model(t), data(t)) for t in trange)) / nsamples * tstop
    return area_mismatch / normalisation
end

function symmetrically_normalised_integral_loss(model_vec::Vector{<:DiffEqBase.AbstractODESolution}, data_vec::Vector{<:DiffEqBase.AbstractODESolution}; kwargs...)
    @assert length(model_vec) == length(data_vec)
    areas = [symmetrically_normalised_integral_loss(t->model_vec[i](t; idxs=2), t->data_vec[i](t; idxs=2); tstop=data_vec[i].t[end], kwargs...) for i in eachindex(model_vec)]
    return mean(areas)
end

function symmetrically_normalised_integral_loss(p, model, inputs, data_sols::Vector{<:DiffEqBase.AbstractODESolution}; kwargs...)
    @assert length(inputs) == length(data_sols)
    try
        model_sols = [simulate(model, inputs[i], p; tstop=data_sols[i].t[end], kwargs...) for i in eachindex(inputs)]
        any(getfield.(model_sols, :retcode) .!= :Success) && return Inf
        return symmetrically_normalised_integral_loss(model_sols, data_sols; kwargs...)
    catch e 
        e isa InterruptException && rethrow(e)
        @warn("Simulate failed, assigning infinite cost to this parameter proposal.")
        return Inf
    end
end

function normalised_integral_loss(model::Function, data::Function; tstop=error("the kwarg tstop needs to be defined."), rtol=1e-3, kwargs...)
    area_mismatch, err = quadgk(t->abs(model(t)-data(t)), 0, tstop; rtol=rtol)
    data_area, _, = quadgk(data, 0, tstop; rtol=rtol)
    return area_mismatch / data_area
end

function normalised_integral_loss(model_vec::Vector{<:DiffEqBase.AbstractODESolution}, data_vec::Vector{<:DiffEqBase.AbstractODESolution}; kwargs...)
    @assert length(model_vec) == length(data_vec)
    areas = [normalised_integral_loss(t->model_vec[i](t; idxs=2), t->data_vec[i](t; idxs=2); tstop=data_vec[i].t[end], kwargs...) for i in eachindex(model_vec)]
    return mean(areas)
end

function normalised_integral_loss(p, model, inputs, data_sols::Vector{<:DiffEqBase.AbstractODESolution}; kwargs...)
    @assert length(inputs) == length(data_sols)
    try
        model_sols = [simulate(model, inputs[i], p; tstop=data_sols[i].t[end], kwargs...) for i in eachindex(inputs)]
        any(getfield.(model_sols, :retcode) .!= :Success) && return Inf
        return normalised_integral_loss(model_sols, data_sols; kwargs...)
    catch e 
        e isa InterruptException && rethrow(e)
        @warn("Simulate failed, assigning infinite cost to this parameter proposal.")
        return Inf
    end
end

function nr_params(s::Symbol)
    s == :cascade_1 && return 3*1
    s == :cascade_2 && return 3*2
    s == :mm_1 && return 2*1 + 1
    s == :mm_2 && return 2*2 + 1
    nr_params(get_model(s))
end
nr_params(::FixedRateCascadeModel) = 4
nr_params(::FixedRateMMModel) = 4


function opt_multi(p::Result, model_keys::Vector{Symbol}; overwrite=false, kwargs...)  
    # if !haskey(p.fitted_models, model_key) && !overwrite
    opt_model_keys = overwrite ? model_keys : filter(!in(key, keys(p.fitted_models)), model_keys)
    if isempty(opt_model_keys) 
        @warn "$model_key already optimised, ignoring."
        return p
    end
    p_new = opt_multi(p.model, opt_model_key, p.param; kwargs...)
    return p = Result(p.model, p.param, p.inputs, merge(p.fitted_models, p_new.fitted_models), p.tstop)
end

function opt_multi(true_model::CascadeModel, model_keys::Vector{Symbol}; 
                   inputs = [PulseInput(10. ^ i, 1.) for i in -3:3:3], tstop = 5000., 
                   min_nodes=1, max_nodes=25, recursion_counter = 0, kwargs...)
    nr_data_nodes = rand(min_nodes:max_nodes)
    data_param = transform_data_param(true_model, PulseInput(), 
                                      rand(nparams(true_model, nr_data_nodes))) 
    data_sols = [simulate(true_model, inputs[i], data_param; tstop=tstop, 
                          callback = TerminateSteadyState( 1e-8, 1e-6), kwargs...) 
                 for i in eachindex(inputs)]


    # Did the simulations succeed?
    success = all((sol.retcode ∈ [:Success, :Terminated] for sol in data_sols))

    # Did the saturation plateau end before the end of the simulation?
    y_max = [get_y_max(true_model, input, data_param) for input in inputs]
    all(y_max .< 1e-4) && (success = false)

    # Is the last value of the last step relatively close to zero?
    last_values = [sol.u[end][end] for sol in data_sols]
    all( - y_max ./ 1e3 .< last_values .< y_max ./ 10) || (success = false)

    if success 
        return opt_multi(true_model, model_keys, data_param, data_sols; inputs=inputs, kwargs...)
    else
        @warn "The generated data had some issue so a new data set was generated."
        recursion_counter > 1e2 && error("The data generation was repeatedly unsuccessful.")
        return opt_multi(true_model, model_keys, data_param, data_sols; inputs=inputs, 
                         recursion_counter = recursion_counter + 1, kwargs...)
    end

    println("Generated target data with $nr_data_nodes steps.")
end

# function opt_multi(true_model::AbstractModel, model_key::Symbol, data_param; nr_params=nr_params(model_key), tstop=5000., silent=false, max_steps=Int(nr_params*3e3), kwargs...)
function opt_multi( true_model::AbstractModel, model_keys::Vector{Symbol}, data_param; 
                   inputs = [PulseInput(10. ^ i, 1.) for i in -3:3:3], tstop=5000., 
                   kwargs...,)
    data_sols = [simulate(true_model, inputs[i], data_param; tstop=tstop, 
                          callback = TerminateSteadyState( 1e-8, 1e-6), kwargs...) 
                 for i in eachindex(inputs)]
    return opt_multi( true_model::AbstractModel, model_keys::Vector{Symbol}, data_param, 
                     data_sols; inputs = inputs, kwargs...,)
end
function opt_multi(true_model::AbstractModel, model_keys::Vector{Symbol}, data_param, 
                   data_sols; inputs = [PulseInput(10. ^ i, 1.) for i in -3:3:3], 
                   silent=false, max_steps=nothing, kwargs...)
    results = Dict{Symbol, Tuple}()
    for model_key in model_keys
        model = get_model(model_key)
        
        #
        res = bboptimize(
            p -> symmetrically_normalised_integral_loss(LinearNodes.transform_param(model, p), model, inputs, data_sols; kwargs...);
            SearchRange=(0., 1.),
            Method=:adaptive_de_rand_1_bin_radiuslimited,
            NumDimensions=nr_params(model_key),
            MaxSteps=max_steps == nothing ? Int(nr_params(model_key)*3e3) : max_steps,
            # MinDeltaFitnessTolerance = 0,
            TraceInterval=5,
            TraceMode= silent ? :silent : :compact
        )
        p_opt = LinearNodes.transform_param(model, best_candidate(res))
        cost = best_fitness(res)
        results[model_key] = (p_opt, cost)
    end
    return ResultMulti(true_model, data_param, inputs, results, [sol.t[end] for sol in data_sols])
end


################################################################################
###############################  Cost functions  ###############################
################################################################################
"""
    normalized_integral_cost(model, input, parameters, x_data, y_data, tstop)

Return the cost for a given parameter set.
;
This is basically a simple interface to the CostFunctions.jl package which
supplies `normalized_integral_cost`.
"""
function normalized_integral_cost(model::AbstractModel, input::AbstractInput, param, x_data, y_data, tstop; kwargs...)
    try
        sol = simulate(model, input, param; tstop=tstop, force_dtmin=true, kwargs...)
        sol.retcode != :Success && return Inf
        solution(t) = sol(t; idxs=length(sol.u[1]))
        return CostFunctions.normalized_integral_cost(
            solution,
            x_data,
            y_data,
            # 1000. / tstop ## This was a bug that I lived with for a long time. It will have a small impact on the cost values stored in the Results types and re-evaluated costs.
            tstop / 100. 
        )
    catch e
        e isa InterruptException && rethrow(e)
        @warn("Simulation errored, assigning infinite cost to that parameter set.")
        return Inf
    end
end

function normalized_integral_cost(model::FixedRateModel, input::ImpulseInput, param, x_data, y_data, tstop; kwargs...)
    solution = simulate(model, input, param)
    CostFunctions.normalized_integral_cost(
        solution,
        x_data,
        y_data,
        # 1000. / tstop ## This was a bug that I lived with for a long time. It will have a small impact on the cost values stored in the Results types and re-evaluated costs.
        tstop / 100. 
    )
end

function normalized_integral_cost(model::AbstractFunctionalModel, input::AbstractInput, param, x_data, y_data, tstop; kwargs...)
    solution = simulate(model, input, param)
    CostFunctions.normalized_integral_cost(
        solution,
        x_data,
        y_data,
        # 1000. / tstop ## This was a bug that I lived with for a long time. It will have a small impact on the cost values stored in the Results types and re-evaluated costs.
        tstop / 100. 
    )
end

function l2_cost(model::AbstractModel, input::AbstractInput, param, x_data, y_data, tstop; kwargs...)
    sol = simulate(model, input, param; tstop=tstop, kwargs...)
	solution(t) = sol(t; idxs=length(sol.u[1]))
    return mean((solution.(x_data) .- y_data) .^ 2) / mean(y_data)^2
end


function l2_cost(model::Union{GammaModel, FixedRateModel}, input::ImpulseInput, param, x_data, y_data, tstop; kwargs...)
    solution(t) = simulate(model, input, param)
    return mean((solution.(x_data) .- y_data) .^ 2) / mean(y_data)^2
end
