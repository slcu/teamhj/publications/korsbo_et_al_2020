@recipe function f(pc::AbstractParameterCollection)
    for p in pc
        @series begin
            p
        end
    end
end


@recipe function f(
                   p::AbstractParameter; 
                   datastyle=:path,
                   datacolor=:black,
                   tstop=maximum(get_tstops(p)),
                   tstop_factor=1,
                   # plotmodels=[:input, :data, 2, :fixed_rate],
                   exclude_input = false, 
                   plotmodels = exclude_input ? [:data, fitted_models(p)...] : [:input, :data, fitted_models(p)...],
                   inputs = get_inputs(p), 
                   plotcolors=Dict(),
                   k_min=3,
                   k_max=5,
                   k_ideal=3,
                   ## Dummy variables to avoid method errors in composit plots
                   percent_sign=nothing,
                   alg = AutoTsit5(Rosenbrock23()),
                   scale_input = 1,
                  )
    xguide --> "Time"
    yguide --> "Concentration"

    xticks, lim_min, lim_max = Plots.optimize_ticks(0., tstop*tstop_factor; k_min=k_min, k_max=k_max, k_ideal=k_ideal, strict_span=true)
    xlims --> (lim_min, lim_max)
    xticks --> xticks

    # ylims --> (0., Inf)
    # xlims --> (0., Inf) 
    grid --> false

    for input in inputs
        data_sol = simulate(p.model, input, p.param; tstop=tstop, alg=alg, scale_input=scale_input, force_dtmin=true, dt = 0.1)
        if :input in plotmodels
            @series begin
                markersize --> 2.
                primary := true
                seriescolor --> (haskey(plotcolors, :input) ? plotcolors[:input] : :grey)
                linewidth --> 1
                seriesalpha --> 0.5
                # linestyle := [:solid :dash]
                label --> "Input" 
                if data_sol isa DESolution
                    plotdensity --> 1000
                    vars := 1
                    data_sol
                else
                    (data_sol.t, hcat(data_sol.u...)[1, :])
                end
            end
        end
        if :data in plotmodels
            @series begin
                markersize --> 2
                primary := true
                linewidth --> 4
                # linestyle := [:solid :dash]
                seriestype --> datastyle
                seriescolor --> (haskey(plotcolors, :data) ? plotcolors[:data] : datacolor)
                label --> "Data"
                if data_sol isa DESolution && datastyle == :path
                    plotdensity --> 1000
                    vars := 2
                    data_sol
                else
                    (data_sol.t, hcat(data_sol.u...)[end, :])
                end
            end
        end

        for (i, model_name) in enumerate(plotmodels)
            model_name === nothing && continue
            model_name = standardise_fixed_step_name(model_name)
            if hasmodel(p, model_name)
                model = get_model(model_name)
                sol = simulate(model, input, param(p, model_name); tstop=tstop,  alg=alg, scale_input=scale_input, force_dtmin=true, dt = 0.1)
                @series begin
                    seriestype --> :path
                    linewidth --> 2
                    primary := true
                    # label --> replace(string(model_name), '_' => ' ')
                    seriesalpha --> 0.85
                    label --> LinearNodes.name(model_name)
                    seriescolor --> (haskey(plotcolors, model_name) ? plotcolors[model_name] : i)
                    if sol isa DESolution
                        plotdensity --> 1000
                        vars --> 2
                        sol
                    else
                        xrange = range(0, tstop, length=300)
                        xrange, sol
                    end
                end
            end
        end
    end

end


@userplot CostComparison
@recipe function f(
                   h::CostComparison;
                   p_highlight = [],
                   highlightcolor = 2,
                   percent_sign = Plots.backend_name() == :pgfplots ? "\\%" : "%",
                   ## Dummy variables to avoid method errors in composit plots
                   datastyle=nothing,
                   datacolor=nothing,
                   tstop=nothing,
                   plotmodels=nothing,
                   cost_scale=:identity
                  )
    if length(h.args) == 3 && h.args[1] isa AbstractParameterCollection
        pc, model, comparison = h.args
    else
        @error("costcomparison takes three arguments; a `ParamCollection`, 
               the model symbol for the x-axis and the model symbol for the y-axis,
               eg. :two_node, :fixed_rate.")
    end


    model_costfield = Symbol("$(model)_cost")
    model_cost = pc[model_costfield]

    comparison_costfield = Symbol("$(comparison)_cost")
    comparison_cost = pc[comparison_costfield]
    if cost_scale == :log10
        model_cost = log10.(model_cost)
        comparison_cost = log10.(comparison_cost)
    end

    lim = ceil(maximum([model_cost; comparison_cost]); sigdigits=1)
    min_cost = floor(minimum([model_cost; comparison_cost]); sigdigits=1)

    seriestype := :scatter
    # ylims --> (Inf, lim)
    # xlims --> (Inf, lim)
    # cost_scale == :log10 && (xticks := -10:10)
    # cost_scale == :log10 && (yticks := -10:10)
    ticks, lim_min, lim_max = Plots.optimize_ticks(min_cost, lim; k_min=3, k_max=5, k_ideal=3, strict_span=true)
    lims --> (lim_min, lim_max)
    ticks --> ticks

    xguide = replace("$model cost", "_"=>"-")
    xguide = replace(xguide, "node"=>"step")
    model == :dde && (xguide = "DDE cost")

    yguide = replace("$comparison cost", "_"=>"-")
    yguide = replace(yguide, "node"=>"step")

    if cost_scale == :log10
        xguide *= Plots.backend_name() in [:pgfplots, :pgfplotsx] ? ", \$\\log_{10}\$" : ", log10"
        yguide *= Plots.backend_name() in [:pgfplots, :pgfplotsx] ? ", \$\\log_{10}\$" : ", log10"
    end
    xguide --> uppercasefirst(xguide) 
    yguide --> uppercasefirst(yguide)
    grid --> false
    legend --> false
    aspect_ratio --> :equal

    percentage_above = round(Int, 100 * sum(model_cost .< comparison_cost)/length(pc))


    xy_above = min_cost .+ (0.3, 0.66) .* (lim - min_cost)
    xy_below = min_cost .+ (0.7, 0.33) .* (lim - min_cost)
    

    annotations --> [
                  (xy_below..., "$(100-percentage_above)$(percent_sign)"),
                  (xy_above..., "$(percentage_above)$(percent_sign)")
                 ]

    @series begin
        primary --> true
        model_cost, comparison_cost
    end
    @series begin
        primary := false
        seriestype := :path
        linecolor := :grey
        range(min_cost, lim, length=10), x->x
    end
    if !isempty(p_highlight)
        p_highlight isa AbstractParameter && (p_highlight = [p_highlight])

        x = getfield.(p_highlight, model_costfield)
        y = getfield.(p_highlight, comparison_costfield)
        if cost_scale == :log10 
            x = log10.(x)
            y = log10.(y)
        end
        @series begin
            primary := false
            markersize --> 6
            markeralpha := 1
            seriescolor := highlightcolor
            x, y
        end
    end
end

@userplot PathwayLengthCost
@recipe function f(
                   h::PathwayLengthCost;
                   comparison = nothing,
                   p_good = [],
                   p_bad = [],
                   max_nodes = 50,
                  )
    if length(h.args) == 2 && h.args[1] isa AbstractParameterCollection && h.args[2] isa Symbol
        pc, model = h.args
    else
        @error("pathwaylengthcost takes two arguments; a `ParamCollection` and a symbol that specifies the model of interest, i.e. `:two_node`.")
    end


    max_nodes != 50 && (pc = @filter(pc, length(param) <= max_nodes + 1))

    model_costfield = Symbol("$(model)_cost")
    # ylims --> (0., 1)
    markeralpha --> 0.25
    markerstrokealpha --> 0.20

    yguide --> "Cost value"
    xguide --> "Pathway length, \$n_{data}\$"

    if !isnothing(comparison)
        comparison_costfield = Symbol("$(comparison)_cost")
        @series begin
            seriestype := :scatter
            markersize --> 2
            seriescolor --> :grey
            label --> ""
            length.(pc) .- 1, pc[comparison_costfield]
        end
    end
    @series begin
        seriestype := :scatter
        label --> ""
        markersize --> 4
        seriescolor --> 1
        length.(pc) .- 1, pc[model_costfield]
    end
end

@userplot CostDisplay
@recipe function f(
                   h::CostDisplay;
                   comparison = nothing,
                   max_nodes = 50,
                   ylims_cost = (-0.08, Inf),
                   best_trajectories = max_nodes == 50 ? [2,5,10,25,50] : [2,3,5,10,25],
                   worst_trajectories = max_nodes == 50 ? [1,5,10,25,50] : [1,3,5,10,25],
                   homogeneous_param=[],
                   datastyle = :path,
                   k_min=3,
                   k_max=5,
                   k_ideal=3,
                   tstop_factor = 1,
                   annotation_y_shifts = fill(0, 100),
                  )
    if length(h.args) == 2 && h.args[1] isa AbstractParameterCollection && h.args[2] isa Symbol
        pc, model = h.args
    else
        @error("costdisplay takes two arguments; a `ParamCollection` and a symbol that specifies the model of interest, i.e. `:two_node`.")
    end
    layout --> @layout [grid(1, length(worst_trajectories)); a{0.45h}; grid(1, length(best_trajectories))]

    model_costfield = Symbol("$(model)_cost")

    pc_good = ParamCollection([])
    pc_bad = ParamCollection([])

    left_margin --> -1mm
    right_margin --> -1mm
    top_margin --> -1mm
    titlelocation  -->  (0.5, 0.75)

    title --> permutedims(["$(subfig)" for subfig in 'A':'Z'])

    @series begin
        subplot := length(worst_trajectories) + 1
        comparison := comparison
        max_nodes --> max_nodes
        PathwayLengthCost((pc, model))
    end


    if !isempty(homogeneous_param)
        @series begin
            subplot := length(worst_trajectories) + 1
            marker --> :star
            seriestype := :scatter
            label --> ""
            markersize --> 4
            seriescolor --> :grey
            (length.(homogeneous_param)[1:max_nodes] .- 1, getfield.(homogeneous_param, model_costfield)[1:max_nodes])
        end
    end

    for (i, ndata) in enumerate(worst_trajectories)


        if isempty(homogeneous_param) 
            ind = argmax(getfield.(@filter(pc, length(param) == ndata + 1), model_costfield))
            p_worst = @filter(pc, length(param) == ndata + 1)[ind]
        else
            p_worst = @filter(homogeneous_param, length(param) == ndata + 1)[1]
        end
        push!(pc_bad, p_worst)

        @series begin
            legend --> false
            plotmodels := p_worst.input isa ImpulseInput ? [:data, comparison] : [:input, :data, comparison]
            plotcolors := Dict(:input => :grey, :data => :black, model=>2, comparison=>:grey)
            subplot := i
            datastyle := datastyle
            tstop_factor := tstop_factor
            k_min:=k_min
            k_max:=k_max
            k_ideal:=k_ideal
            # datastyle --> :scatter
            i != 1 && (yguide := "")
            p_worst
        end
        @series begin
            legend --> false
            plotmodels := [model]
            plotcolors := Dict(:input => :grey, :data => :black, model=>2, comparison=>:grey)
            subplot :=  i
            datastyle := datastyle
            tstop_factor := tstop_factor
            k_min:=k_min
            k_max:=k_max
            k_ideal:=k_ideal
            # datastyle --> :scatter
            i != 1 && (yguide := "")
            p_worst
        end
        @series begin
            subplot := length(worst_trajectories) + 1
            seriestype := :scatter
            seriescolor --> 2
            label := ""
            [ndata], [getfield(p_worst, model_costfield)]
        end
    end


    for (i, ndata) in enumerate(best_trajectories)

        ind = argmin(getfield.(@filter(pc, length(param) == ndata + 1), model_costfield))
        p_best = @filter(pc, length(param) == ndata + 1)[ind]
        push!(pc_good, p_best)

        @series begin
            legend --> false
            plotmodels := p_best.input isa ImpulseInput ? [:data, comparison] : [:input, :data, comparison]
            plotcolors := Dict(:input => :grey, :data => :black, model=>3, comparison=>:grey)
            subplot := length(worst_trajectories) + 1 + i
            datastyle := datastyle
            k_min:=k_min
            k_max:=k_max
            k_ideal:=k_ideal
            tstop_factor := tstop_factor
            # datastyle --> :scatter
            i != 1 && (yguide --> "")
            p_best
        end
        @series begin
            legend --> false
            plotmodels := [model]
            plotcolors := Dict(:input => :grey, :data => :black, model=>3, comparison=>:grey)
            subplot := length(worst_trajectories) + 1 + i
            datastyle := datastyle
            tstop_factor := tstop_factor
            k_min:=k_min
            k_max:=k_max
            k_ideal:=k_ideal
            # datastyle --> :scatter
            i != 1 && (yguide --> "")
            p_best
        end
        @series begin
            subplot := length(worst_trajectories) + 1 
            seriestype := :scatter
            seriescolor --> 3
            label := ""
            ylims --> (0., 1)
            [ndata], [getfield(p_best, model_costfield)]
        end
    end

    @series begin
        subplot := length(worst_trajectories) + 1 
        label := ""
        yguide --> "Cost value"
        xguide --> "Pathway length, \$n_{data}\$"
        ylims --> ylims_cost
        annotations := vcat(
                         [
                          (length(p.param) - 1 + 0., getfield(p, model_costfield) + 0.12 + annotation_y_shifts[i], text("$(('A':'Z')[i])", 11)) for (i, p) in enumerate(pc_bad)
                         ],
                         [
                          (length(p.param) - 1 + 0., getfield(p, model_costfield) - 0.13, text("$(('A':'Z')[i+length(pc_bad)+1])", 11)) for (i, p) in enumerate(pc_good)
                         ])
        [],[]
    end

end


@userplot MultiInputPlot
@recipe function f(
                   h::MultiInputPlot; 
                   model = :two_node,
                   comparison = :fixed_rate,
                   cost_scale=:identity,
                   plotcolors = Dict(),
                  )
    if length(h.args) == 1 && h.args[1] isa AbstractParameterCollection
        pc = h.args[1]
    else
        @error("multiinputplot takes only one argument; a `ParamCollection`")
    end
    percent_sign = Plots.backend_name() == :pgfplots ? "\\%" : "%"
    nrows = 3
    ncols = 4
    # layout := grid(nrows, ncols, widths=[0.2, 0.4, 0.4])
    # layout := grid(nrows, ncols, widths=[0.4, 0.6])
    # layout := grid(nrows, ncols, widths=[0.20, 0.3, 0.2, 0.3])
    layout := (nrows, ncols)
    size --> (600, 400)
    grid --> false
    legend --> false

    model_costfield = Symbol("$(model)_cost")
    model_paramfield = Symbol("$(model)_param")

    comparison_costfield = Symbol("$(comparison)_cost")
    comparison_paramfield = Symbol("$(comparison)_param")
    subplot = 1

    for (i, input_type) in enumerate([
                                      ImpulseInput,
                                      PiecewiseInput,
                                      StepInput,
                                      RampInput,
                                      WaveInput,
                                      RepeatedNoiseInput,
                                     ])
        pc_filtered = @filter pc input isa input_type
        
        # _, worst_model_ind = findmax(pc_filtered[model_costfield])
        # _, worst_comparison_ind = findmax(pc_filtered[comparison_costfield])
        p_median = geometric_median(pc_filtered, model_costfield, comparison_costfield)
        @series begin
            # subplot := ncols * (i - 1) + 1 
            subplot := subplot
            subplot += 1
            primary := false
            cost_scale := cost_scale
            seriescolor --> 1
            # p_highlight --> [pc_filtered[worst_model_ind],
            #                  pc_filtered[worst_comparison_ind]]
            # highlightcolor --> [2, 3]
            p_highlight --> p_median
            highlightcolor --> 2
            subplot <= nrows * (ncols - 1) && (xguide := "")

            LinearNodes.CostComparison((pc_filtered, model, comparison))
        end

        @series begin
            # subplot := ncols * (i - 1) + 2
            subplot := subplot
            subplot += 1
            plotmodels := input_type == ImpulseInput ? [:data, model, comparison] : [:input, :data, model, comparison]
            subplot <= nrows * (ncols - 1) && (xguide := "")
            plotcolors := plotcolors
            # pc_filtered[worst_model_ind]
            p_median
        end
        # @series begin
        #     subplot := 3i 
        #     xguide := i == 5 ? "Time" : ""
        #     plotmodels := [:input, :data, model, comparison]
        #     pc_filtered[worst_comparison_ind]
        # end
    end
end

@userplot DDEPlot
@recipe function f(h::DDEPlot; cost_scale=:identity, model=:dde, comparison1=:fixed_rate, comparison2=:two_node, plotcolors=Dict())
    if length(h.args) == 1 && h.args[1] isa AbstractParameterCollection
        pc = h.args[1]
    else
        @error("ddeplot takes only one argument; a `ParamCollection`")
    end
    size --> (600, 400)

    layout := grid(3, 3, widths=[0.25, 0.5, 0.25])
    for (i, input_type) in enumerate([
                                      ImpulseInput,
                                      StepInput,
                                      #          RampInput,
                                      #          WaveInput,
                                      RepeatedNoiseInput,
                                     ])
        pc_filtered = @filter pc input isa input_type

        p_median = geometric_median(pc_filtered, :dde_cost, Symbol("$(comparison1)_cost"))

        @series begin
            subplot := 3i - 2
            primary := true
            p_highlight := p_median
            i != 3 && (xguide --> "")
            cost_scale --> cost_scale
            p_highlight --> [p_median]
            LinearNodes.CostComparison((pc_filtered, model, comparison1))
        end
        @series begin
            tstop = input_type == RepeatedNoiseInput ? p_median.tstop : p_median.tstop/2
            tstop --> tstop
            subplot := 3i - 1
            # seriescolor := input_type == ImpulseInput ? [:black 1 3 7] : [:grey 1 3 7] 
            # plotmodels := input_type == ImpulseInput ? [:data, model, comparison1, comparison2] : [:data, :input, model, comparison1, comparison2]

            plotmodels := [:data, :input, model, comparison1, comparison2]
            plotcolors := plotcolors
            # seriescolor --> [1 1 1 :grey :black 1 2 3 7 2 2]
            # seriescolor --> (input_type == ImpulseInput ? [:black 3 1 2] : [:grey :black 3 1 2])
            legend := false
            p_median
        end
        @series begin
            subplot := 3i 
            primary := true
            p_highlight := p_median
            i != 3 && (xguide --> "")
            cost_scale --> cost_scale
            p_highlight --> p_median
            LinearNodes.CostComparison((pc_filtered, model, comparison2))
        end
    end
end



@userplot SharpnessPlot
@recipe function f(
                   h::SharpnessPlot;
                   p_highlight = [],
                   plotmodels = [:data, 2],
                   highlightcolor = :white,
                   highlightsize = 6,
                   highlightshape = :diamond,
                  )
    if length(h.args) == 1 && h.args[1] isa AbstractParameterCollection
        pc = h.args[1]
    else
        @error("sharpnessplot takes one arguments, a `ParamCollection`")
    end
    ylims --> (0., 6)
    xlims --> (1., Inf)
    seriestype := :scatter
    yguide --> L"\textrm{Sharpness, } s"
    xguide --> L"n_{data}"
    if :data in plotmodels
        @series begin
            label --> "Data"
            seriescolor --> :black
            seriesalpha --> 0.3
            length.(pc[:param]) .- 1,
            sharpness.(pc)
        end
    end

    for n in plotmodels
        if n in 2:5
            @series begin
                label --> "$n-step model"
                seriesalpha --> 0.3
                seriescolor --> n
                length.(pc[:param]) .- 1,
                sharpness.(pc, n)
            end
        end
    end
    for p in p_highlight
        @series begin
            label := ""
            seriescolor --> highlightcolor
            shape --> highlightshape
            markersize --> highlightsize
            fill(length(p) -1, sum(isa.(plotmodels, Number)) +1)', 
            vcat(sharpness(p), [sharpness(p, n) for n in plotmodels if n isa Number])'
        end
    end
    #===========================  shade out-of-bounds  ============================# 
    seriestype := :path
    @series begin
        fillrange := 10
        linealpha := 0.
        fillalpha --> 0.2
        seriescolor --> :black
        label --> ""
        x->sqrt(x),
        0:0.1:50
    end
    @series begin
        primary := false
        linealpha := 0.
        seriescolor --> :black
        fillrange := 0
        fillalpha --> 0.2
        x->1.,
        0:0.1:50
    end
end


@userplot PredictionPlot
@recipe function f(
                   h::PredictionPlot;
                   idx_highlight = 0,
                   highlightshape = :diamond,
                   opt_targets = [1.],
                  )
    if length(h.args) == 3 
        cost_matrix = h.args[1]
        mask = h.args[2]
        scales = h.args[3]
    else
        @error("Wrong arguments to predictionplot, check the code in src/plot_recipes for guidence.")
    end
    ylims --> (0., 1.)
    legend --> :topright
    xguide --> "Input scaling"
    @series begin
        x = scales
        y = permutedims(mean(cost_matrix[mask, :], dims=1))
        xscale --> :log10
        lw --> 4,
        seriescolor --> [1 2]
        x, y
    end
    @series begin
        primary --> false
        x = scales
        y = permutedims(mean(cost_matrix[mask, :], dims=1).- std(cost_matrix[mask, :], dims=1))
        fillrange --> permutedims(mean(cost_matrix[mask, :], dims=1).+ std(cost_matrix[mask, :], dims=1))
        fillalpha --> 0.2
        linealpha --> 0
        x,y
    end
    if idx_highlight != 0
        @series begin
            x = scales
            y = cost_matrix[idx_highlight, :]
            @show y
            linecolor --> :black
            linewidth --> 1
            x, y
        end
    end
    @series begin
        seriestype := :vline
        seriescolor := :black
        seriesalpha := 0.1
        label := ""
        opt_targets
    end
end
