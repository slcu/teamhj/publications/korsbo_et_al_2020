x_tot_values(::FixedRateCascadeModel, p) = p[1]
x_tot_values(::CascadeModel, p) = p[3:3:end]

"""
    saturation(model::AbstractModel, input::AbstractInput, p::AbstractParameter; kwargs...)

get the saturation degree [0, 1] of the most saturated variable in a simulation.
"""
saturation(model::AbstractModel, input::AbstractInput, p::AbstractParameter; kwargs...) = saturation(model, input, p.param; tstop=p.tstop, kwargs...)
function saturation(model::AbstractModel, input::AbstractInput, p; tstop=5e3, kwargs...)
    sol = simulate(
                    model,
                    input,
                    p;
                    tstop = tstop,
                    alg = Tsit5(),
                    save_vars = :all,
                )
    γ = x_tot_values(model, p)
    trange = range(0.0, stop = tstop, length = 500)
    saturation_score = 1 - minimum( (γ .- maximum(hcat(sol(trange)...)[2:end, :]; dims = 2)) ./ γ )
    return saturation_score
end
saturation(p; input = get_inputs(p)[round(Int, end/2)], kwargs...) = saturation(p.model, input, p; kwargs...)
saturation(p, model_key::Symbol; kwargs...) = saturation(get_model(model_key), p.input, param(p, model_key); kwargs...)


evaluate_cost(p::AbstractParameter, model::Symbol; kwargs...) = evaluate_cost(p, model, p.input; kwargs...)
function evaluate_cost(p::AbstractParameter, model_key::Symbol, input::AbstractInput; tstop=p.tstop, kwargs...)
    try
        target = simulate(p.model, input, p.param; tstop=tstop)
        # x_data, y_data, tstop = LinearNodes.generate_target(p; kwargs...)
        t = range(0., stop=tstop, length=300)
        y = target.(t; idxs=2)
        model = simulate(get_model(model_key), input, param(p, model_key); tstop = tstop)
        return LinearNodes.symmetrically_normalised_integral_loss([model], [target]; kwargs...)
        # return LinearNodes.normalized_integral_cost(get_model(model), input, param(p, model), t, y, tstop; kwargs...)
    catch e
        e isa InterruptException && rethrow(e)
        # @warn "Caught an error during evaluate_cost. Returning infinite cost value."
        return Inf
    end
end




# function evaluate_cost(p::AbstractParameter, model::Symbol, input::AbstractInput; kwargs...)
#     target = simulate(p.model, input, p.param; tstop=p.tstop)
#     sol = simulate(get_model(model), input, param(p, model); tstop=p.tstop)
#     LinearNodes.normalised_integral_loss(t -> sol(t; idxs=2), t -> target(t; idxs=2); tstop=p.tstop, kwargs...)
# end

get_y_max(p::AbstractParameter; kwargs...) = get_y_max(p.model, p; kwargs...)
get_y_max(model::AbstractModel, p; kwargs...) = get_y_max(model, Pulse(), p; kwargs...)
function get_y_max(model::AbstractModel, input::AbstractInput, p; kwargs...)
    try
        sol = simulate(model, input, p; tstop=5000., callback=TerminateSteadyState( 1e-8, 1e-6), kwargs...)
        sol.retcode in [:Success, :Terminated] || return -Inf
        res = optimize(x -> - sol(x; idxs=length(sol.u[end])), 0., sol.t[end])
        y_max = - minimum(res)
        # y_max = maximum(x -> sol(x; idxs=length(sol.u[end])), range(0., stop=sol.t[end], length=2000))

    return y_max
    catch e
        display("p = $p")
        if e isa DiffEqBase.LinearAlgebra.SingularException 
            return -Inf
        else
            rethrow(e)
        end
    end
end


saturation_integral(args...; kwargs...) = saturation_summary(args...; kwargs...)[1]
saturation_summary(p::AbstractParameter; kwargs...) = saturation_summary(p.model, p; kwargs...)
function saturation_summary(model::AbstractModel, p; kwargs...)
    try
        sol = simulate(model, PulseInput(), p; tstop=5000., callback=TerminateSteadyState( 1e-8, 1e-6), maxiters=1e6, kwargs...)
        res = optimize(x -> - sol(x; idxs=length(sol.u[end])), 0., sol.t[end])
        y_max = - minimum(res)
        # search_range = range(0., stop=sol.t[end], length=2000)
        # y_max = maximum(x -> sol(x; idxs=length(sol.u[end])), search_range)
        # t_half_1 = maximum(x -> sol(x; idxs=length(sol.u[end])), range(0., stop=sol.t[end], length=2000))
        # t_half_1 = search_range[findfirst(sol.(search_range; idxs=length(sol.u[end])) .> y_max/2)
        # t_half_2 = argmin(sol.(search_range; idxs=length(sol.u[end])))
        #
        condition(u, t, i) = u[end] - y_max / 2.
        t_half_1 = 0.
        affect!(i) = (t_half_1 = i.t)
        cb = ContinuousCallback(condition, affect!, terminate!)
        # cbset = CallbackSet(cb, PositiveDomain()) 
        cbset = CallbackSet(cb)
        sol = simulate(model, PulseInput(), p; callback=cbset, kwargs...)
        sol.retcode != :Terminated && return (Inf, Inf, Inf, Inf)
        t_half_2 = sol.t[end]
        int, err = quadgk(t -> sol(t; idxs=2), t_half_1, t_half_2, rtol=1e-3) ./ (y_max * (t_half_2 - t_half_1))
        return (int, y_max, t_half_1, t_half_2)
    catch e
        display("p = $p")
        # if e isa DiffEqBase.LinearAlgebra.SingularException 
            # @warn("Caught a LineraAlgebra.SingularException error!")
            @warn("Caught an error!")
            return (Inf, -Inf, Inf, Inf)
        # else
            # rethrow(e)
        # end
    end
end
