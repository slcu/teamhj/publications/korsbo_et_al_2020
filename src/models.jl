################################################################################
##############################  Type definitions  ##############################
################################################################################
# Here, we define a set of types for different models and inputs. 
# The types do not contain anything but is only used for despatch.
abstract type AbstractModel <: Function end
abstract type AbstractDEModel <: AbstractModel end
abstract type AbstractODEModel <: AbstractDEModel end
abstract type AbstractDDEModel <: AbstractDEModel end
abstract type AbstractFunctionalModel <: AbstractModel end

struct FixedStepModel <: AbstractODEModel end
struct FixedRateModel <: AbstractODEModel end
struct CascadeModel <: AbstractODEModel end
struct FixedRateHillModel <: AbstractODEModel end
struct FixedRateMMModel <: AbstractODEModel end
struct FixedRateCascadeModel <: AbstractODEModel end
struct MMChainModel <: AbstractODEModel end
struct GammaModel <: AbstractFunctionalModel end
struct GammaHillModel <: AbstractFunctionalModel end
struct GammaDDEModel <: AbstractDDEModel end
struct DDEModel <: AbstractDDEModel end
struct GammaGeneralModel <: AbstractFunctionalModel end

abstract type AbstractReversibleModel <: AbstractModel end
struct FixedStepReversibleModel <: AbstractReversibleModel end

abstract type AbstractInput end
struct ImpulseInput <: AbstractInput end
struct DecayingInput <: AbstractInput 
    u0::Float64
    r_decay::Float64
end
DecayingInput() = DecayingInput(1, 1)
struct StepInput <: AbstractInput end
struct WaveInput <: AbstractInput end
struct PiecewiseInput <: AbstractInput end
struct PulseInput <: AbstractInput 
    value::Float64
    duration::Float64
end
PulseInput() = PulseInput(1.0, 1.0) ## Set a default pulse

struct RampInput <: AbstractInput 
    inclination::Float64
end
RampInput() = RampInput(1.0) ## Set a default inclination.

abstract type AbstractNoiseInput <: AbstractInput end
struct NoiseInput <: AbstractNoiseInput end
struct RepeatedNoiseInput{T<:Number} <: AbstractNoiseInput
	positive_jump_times::Vector{T}
	negative_jump_times::Vector{T}
end

function RepeatedNoiseInput(sol::DiffEqBase.AbstractODESolution) 
	RepeatedNoiseInput(
					   find_positive_steps(sol), 
					   find_negative_steps(sol)
					   )
end





# Make instances of the model types callable like functions.
(f::FixedStepModel)(input::AbstractInput, args...; kwargs...) = simulate(f, input, args...; kwargs...)
(f::FixedRateModel)(input::AbstractInput, args...; kwargs...) = simulate(f, input, args...; kwargs...)
(f::GammaModel)(input::AbstractInput, args...; kwargs...) = simulate(f, input, args...; kwargs...)
(f::GammaModel)(args...; kwargs...) = simulate(f, DecayingInput(), args...; kwargs...)

#==============================================================================#
#============================  Input derivatives  =============================# 
#==============================================================================#

"""
    input_derivatives!(du, u, p, t, input::AbstractInput)

Calculate the derivatives for a given input and update du[1] in-place.
"""
function input_derivatives! end

function input_derivatives!(du, u, p, t, input::DecayingInput)
    du[1] = - input.r_decay * u[1]
    return nothing
end


function input_derivatives!(du, u, p, t, input::Union{StepInput, PiecewiseInput, PulseInput, AbstractNoiseInput, ImpulseInput})
    ## These inputs apply their effect in other ways than though a derivative.
    du[1] = 0
    return nothing
end

function input_derivatives!(du, u, p, t, input::RampInput)
    du[1] = input.inclination
    return nothing
end

function input_derivatives!(du, u, p, t, input::WaveInput)
    du[1] = cos(t)
    return nothing
end

#==============================================================================#
#============================  Model derivatives  =============================# 
#==============================================================================#

"""
    model_derivatives!(du, u, p, t, model)

Update du with the derivatives of a given model. 

u[1] is the input
"""
function model_derivatives! end

"""
    model_derivatives!(du, u, p, t, ::FixedStepModel)

Calculate the derivatives for the fixed-step model in-place.
Mutates u0. 

u[1] is the input
p = [γ, r_1, r_2, ..., r_n]
"""
function model_derivatives!(du, u, p, t, ::FixedStepModel)
    for i in 2:(length(du)-1)
        du[i] = p[i] * (u[i-1] - u[i])
    end
    du[end] = p[end] * (p[1] * u[end-1] - u[end])
    return nothing
end

function model_derivatives!(du, u, p, t, ::FixedRateModel)
    for i in 2:(length(du)-1)
        du[i] = p[3] * (u[i-1] - u[i])
    end
    du[end] = p[3] * (p[1] * u[end-1] - u[end])
    return nothing
end


function model_derivatives!(du, u, p, t, ::FixedRateHillModel)
    # p = [γ, n, r, k, n_hill]
    for i in 2:(length(du)-1)
        du[i] = p[3] * (u[i-1] - u[i])
    end
    du[end] = p[3] * (p[1] * abs(u[end-1])^p[5]/(p[4]^p[5] + abs(u[end-1])^p[5]) - u[end])
    return nothing
end

function model_derivatives!(du, u, p, t, ::FixedRateMMModel)
    # p = [γ, n, r, k]
    for i in 2:(length(du)-1)
        # du[i] = p[3] * (u[i-1] - u[i])
        du[i] = p[3] * (u[i-1]/(p[4] + u[i-1]) - u[i])
    end
    du[end] = p[3] * (p[1] * u[end-1]/(p[4] + u[end-1]) - u[end])
    return nothing
end

function model_derivatives!(du, u, p, t, ::MMChainModel)
    # p = [γ, r_1, k_1 ..., r_n, k_n]
    # du[2] = p[3] * (p[2] * u[1] / (p[1] + u[1]) - u[2])
    # du[2] = p[2] * (u[1] - u[2])
    for i in 2:(length(du)-1)
        j = 2*(i - 2) + 2
        du[i] = p[j] * (u[i-1] / (p[j+1] + u[i-1]) - u[i])
    end
    du[end] = p[end-1] * (p[1] * u[end-1] / (p[end] + u[end-1]) - u[end])
    return nothing
end

function model_derivatives!(du, u, h, p, t, ::DDEModel)
    du[2] = p[3] * (p[1] * h(p, t-p[2]; idxs=1) - u[2])
    return nothing
end

function model_derivatives!(du, u, h, p, t, ::GammaDDEModel)
    ## We can't integrate from -infinity, so find a suitable starting time.
    t_start = t - p[2]/p[3] - 3 * sqrt(p[2])/p[3] # t - mean - 3*std
    if h(p, -1; idxs=1) == 0 && t_start < 0
        t_start = 0
    end

    if t == t_start 
        integral = 0.
    else
        integral, err = quadgk(time -> h(p, time, idxs=1) * gamma_model(t - time, 1, p[2]-1, p[3]), t_start, t, rtol=1e-2)
    end
    du[2] = p[3] * (p[1] * integral - u[2])
    return nothing
end


function model_derivatives!(du, u, p, t, ::CascadeModel)
    # p = [α_1, β_1, Xtot_1, ..., α_n, β_n, Xtot_n]
    for i in 2:(length(du))
		j = 3*(i-1) - 2
        # du[i] = p[j]*u[i - 1]*(p[j+2] - u[i]) - p[j+1]*u[i]
        du[i] = p[j+1] * (p[j] * u[i - 1] * (1 - u[i]/p[j+2]) - u[i])
	end
end

function model_derivatives!(du, u, p, t, ::FixedRateCascadeModel)
    # p = [γ, n, α, β]
    # for i in 2:(length(du)-1)
    #     du[i] = p[3]*u[i - 1]*(1 - u[i]) - p[4]*u[i]
	# end
    # du[end] = p[3]*u[end - 1]*(p[1] - u[end]) - p[4]*u[end]
    for i in 2:(length(du))
        du[i] = p[4] * (p[3] * u[i - 1] * (1 - u[i]/p[1]) - u[i])
        # du[i] = p[4] * p[3] * u[i-1] - p[4] * u[i] * (u[i - 1] * p[3] / p[1] + 1)
	end
end

function model_jacobian!(J, u, p, t, ::FixedRateCascadeModel)
    # println("Using bad jac!")
    # p = [γ, n, α, β]
    for i in 2:(length(u))
        J[i,i] = - (p[4] * p[3] / p[1]) * u[i-1] - p[4]
        J[i,i-1] = p[4] * p[3] * (1 - u[i] / p[1])
	end
end

function input_jacobian!(J, u, p, t, ::AbstractInput)
    J[1,1] = 0.
end

function input_jacobian!(J, u, p, t, input::DecayingInput)
    J[1,1] = - input.r_decay
end

function model_derivatives!(du, u, p, t, ::FixedStepReversibleModel)
    # -1    0    1    2
    # r_b, r_u, r_b, r_u
    # activate from input, transfer to u[3], reversed transfer from u[3].
    du[2] = p[1] * u[1] - p[2] * u[2] + p[3] * u[3]
    for i in 3:length(du)-1
        j = 2*(i-2) + 1
        du[i] = p[j-1] * u[i-1] - (p[j] + p[j+1]) * u[i] + p[j+2] * u[i+1]
    end
    j = 2*(length(du) - 2) + 1
    du[end] = p[j-1] * u[end-1] - (p[j] + p[j+1]) * u[end] 
    du[1] += p[2] * u[2] # unbinding of the first node after input.
    du[2] += - p[2] * u[2] # unbinding of the first node after input.
    return nothing
end

#==============================================================================#
#==================  Build differential equation functions  ===================# 
#==============================================================================#

function build_de(model::FixedRateCascadeModel, input::AbstractInput)
    # function f(du, u, p, t) 
    #     input_derivatives!(du, u, p, t, input)
    #     model_derivatives!(du, u, p, t, model)
    #     return nothing
    # end
    # function jac(J, u, p, t)
    #     input_jacobian!(J, u, p, t, input)
    #     model_jacobian!(J, u, p, t, model)
    # end
    # return ODEFunction(f; jac=jac)

    return (du, u, p, t) -> begin
        input_derivatives!(du, u, p, t, input)
        model_derivatives!(du, u, p, t, model)
        return nothing
    end
end

function build_de(model::AbstractODEModel, input::AbstractInput)
    return (du, u, p, t) -> begin
        input_derivatives!(du, u, p, t, input)
        model_derivatives!(du, u, p, t, model)
        return nothing
    end
end

function build_de(model::AbstractDDEModel, input::AbstractInput)
    return (du, u, h, p, t) -> begin
        input_derivatives!(du, u, p, t, input)
        model_derivatives!(du, u, h, p, t, model)
        return nothing
    end
end

################################################################################
############################  Analytical functions  ############################
################################################################################

## The most natural implementation. It is, however, prone to overflowing.
# It is not used, it's mainly around for a mathematical reference + testing.
@inline gamma_model_unsafe(t, p, n, r) = p*r^n*t^(n-1)*exp(-r*t)/gamma(n)

@inline gamma_model_exp_log(t, p, n, r) =
    exp(log(p) + n * log(r) + (n - 1) * log(t) - r * t - log(gamma(n)))

@inline gamma_model_exp_log_approx_gamma(t, p, n, r) = exp(
    log(p) + n * log(r) + (n - 1) * log(t) - r * t -
    (log(sqrt(2π)) + (n - 0.5) * log(n - 1) - (n - 1)),
)
@doc raw"""
    gamma_model(t, p, n, r)

run the gamma model with overflow protection. 

The gamma model is first re-written using x = e^ln(x). With some logarithm
arithmetic, this helps prevent overflow in the numerator. If n is large enough
to start overflowing gamma(n) then the function starts using the approximation
$(n-1)! \approx \sqrt{2\pi}*(n-1)^{n-0.5}*e^{-(n-1)}$.  This allows some
further logarithm arithmetic to avoid overflows for large n. The approximation
introduces an unnoticably small error in the scaling of the output.
"""
@inline function gamma_model(t, p, n, r)
    if n <= 171 ## This is where gamma(n) overflows a double.
        return gamma_model_exp_log(t, p, n, r)
    else
        return gamma_model_exp_log_approx_gamma(t, p, n, r)
    end
end

@doc raw"""
    gamma_hill(t, γ, n, r, k, n_hill=1)

Run the gamma model with an instantaneous non-linear step at the end of the pathway.

## Arguments
- γ - a scaling parameter. It comes from γ \def y_total = y_active + y_inactive
where y_active is the output of this function. It serves the same function as 
γ parameter in the gamma model, but they are not the same!
- n - number of nodes in the linear pathway
- r - rate of each step in the linear pathway
- k - the half-maximum concentration. It comes from $k \def α/β$ where α is the
activation rate and β is the inactivation rate of the output node, y.
- n_hill - hill coefficient. Theoretically unjustified. But useful!
"""
function gamma_hill(t, γ, n, r, k, n_hill=1) 
    Γ = gamma_model(t, 1, n, r)
    γ * Γ^n_hill / (k^n_hill + Γ^n_hill)
end


function gamma_general(input, param, t)
    γ, n, r = param
    input isa ImpulseInput && (n = n - 1)
    integral, err = quadgk(
        τ -> input_value(input, τ, param) * gamma_model(t - τ, γ, n, r),
        tstart(input, t, n, r),
        discontinuities(input, param, t)...,
        t,
        rtol = 1e-4,
    )
    return integral
end

function gamma_general(input::RepeatedNoiseInput, param, t)
    γ, n, r = param

    jumps = vcat(input.positive_jump_times, input.negative_jump_times)
    jump_values = vcat(
                       fill(1, length(input.positive_jump_times)),
                       fill(-1, length(input.negative_jump_times)),
                      )
    input_conc = cumsum(jump_values[sortperm(jumps)])
    jump_times = sort(jumps)

    @inline input_value(τ) = τ < jump_times[1] ? 0 : input_conc[searchsortedlast(jump_times, τ)]

    integral, err = quadgk(
        τ -> input_value(τ) * gamma_model(t - τ, γ, n, r),
        tstart(input, t, n, r),
        discontinuities(input, param, t)...,
        t,
        rtol = 1e-4,
    )
    return integral
end

#============  Bookkeeping for the distributed delay simulations  =============# 
@inline tstart(::AbstractInput, t, n, r) = min(0, t - n/r - 3*sqrt(n)/r)
@inline tstart(::PiecewiseInput, t, n, r) = 0
@inline tstart(::WaveInput, t, n, r) = t - n/r - 3*sqrt(n)/r

@inline discontinuities(::AbstractInput, p, t) = ()
@inline discontinuities(::PiecewiseInput, p, t) = t > 100 ? (100,) : ()
@inline discontinuities(input::PulseInput, p, t) = t > 1 ? (1,) : ()
@inline function discontinuities(input::RepeatedNoiseInput, p, t)
    result = sort(vcat(input.positive_jump_times, input.negative_jump_times))
    result = result[ result .< t]
    return result
end

@inline input_value(::ImpulseInput, t, param) =  t >= 0 ? exp(-t*param[3]) * param[3] : 0
@inline input_value(input::DecayingInput, t, param) =  t >= 0 ? exp(-t*input.r_decay) * input.u0 : 0
@inline input_value(::StepInput, t, param) =  t >= 0 ? 1. : 0
@inline input_value(::WaveInput, t, param) = t >= 0 ? 1. + sin(t) : 1.
@inline input_value(input::RampInput, t, param) = t >= 0 ? input.inclination * t : 0
@inline input_value(::PiecewiseInput, t, param) =  0 <= t <= 100 ? 1 : 0
@inline input_value(input::PulseInput, t, param) =  0 <= t <= input.duration ? input.value : 0


################################################################################
##################  Initial conditions and support functions  ##################
################################################################################

@inline get_u0(args...) = vcat(initial_input(args...), initial_x(args...))

#=================================  Defaults  =================================# 
@inline initial_input(::AbstractModel, ::AbstractNoiseInput, param) = [0.]
@inline initial_input(::AbstractModel, ::ImpulseInput, param) = [0.]
@inline initial_input(::AbstractModel, input::DecayingInput, param) = [input.u0]
@inline initial_input(::AbstractModel, ::WaveInput, param) = [1.]
@inline initial_input(::AbstractModel, ::RampInput, param) = [0.]
@inline initial_input(::AbstractModel, ::StepInput, param) = [1.]
@inline initial_input(::AbstractModel, ::PiecewiseInput, param) = [1.]
@inline initial_input(::AbstractModel, input::PulseInput, param) = [input.value]


#=================================  Defaults  =================================# 
@inline initial_x(::FixedRateModel, ::AbstractInput, param) = fill(0., round(Int, param[2]))
@inline initial_x(::FixedStepModel, ::AbstractInput, param) = fill(0., length(param)-1)
@inline initial_x(::DDEModel, ::AbstractInput, param) = [0.]
@inline initial_x(::GammaDDEModel, ::AbstractInput, param) = [0.]
@inline initial_x(::FixedStepReversibleModel, ::AbstractInput, param) = fill(0., Int((length(param)) / 2) - 1)
@inline initial_x(::CascadeModel, ::AbstractInput, param) = fill(0., Int(length(param)/3))
@inline initial_x(::FixedRateCascadeModel, ::AbstractInput, param) = fill(0., round(Int, param[2]))
@inline initial_x(::FixedRateHillModel, ::AbstractInput, param) = fill(0., round(Int, param[2]))
@inline initial_x(::FixedRateMMModel, ::AbstractInput, param) = fill(0., round(Int, param[2]))
@inline initial_x(::MMChainModel, ::AbstractInput, param) = fill(0., Int((length(param) - 1)/2))

#=====================  Impulse input special treatment  ======================# 
@inline initial_x(::FixedRateModel, ::ImpulseInput, param) = vcat(param[3], fill(0., round(Int, param[2])-1))
@inline initial_x(::FixedStepModel, ::ImpulseInput, param) = vcat(param[2], fill(0., length(param)-2))
@inline initial_x(::CascadeModel, ::ImpulseInput, param) = vcat(param[3]*param[1]/(1+param[1]), fill(0., Int(length(param)/3)-1))
@inline initial_x(::DDEModel, ::ImpulseInput, param) = [0.]
@inline initial_x(::GammaDDEModel, ::ImpulseInput, param) = [0.]
@inline initial_x(::FixedRateHillModel, ::ImpulseInput, param) = vcat(param[3], fill(0., round(Int, param[2])-1))
@inline initial_x(::FixedRateMMModel, ::ImpulseInput, param) = vcat(param[3], fill(0., round(Int, param[2])-1))

@inline initial_x(::FixedRateCascadeModel, ::ImpulseInput, param) = vcat(param[1] * param[3] * param[4] / (param[1] * param[3] + param[4]), fill(0.,round(Int, param[2]-1)))
@inline initial_x(::MMChainModel, ::ImpulseInput, param) = vcat(param[1]*param[2], fill(0., Int((length(param)-1)/2)-1))
# @inline initial_x(::FixedRateCascadeModel, ::ImpulseInput, param) = vcat(param[4]*param[3]/(1+param[3]), fill(0.,round(Int, param[2]-1)))
# @inline initial_x(::FixedRateCascadeModel, ::ImpulseInput, param) = vcat(param[3]/(1+param[3]), fill(0.,round(Int, param[2]-1)))

#=======================  Wave input special treatment  =======================# 
@inline initial_x(::FixedRateModel, ::WaveInput, param) = vcat(fill(1., round(Int, param[2]-1)), param[1])
@inline initial_x(::FixedStepModel, ::WaveInput, param) = vcat(fill(1., length(param)-2), param[1])
# @inline initial_x(::CascadeModel, ::WaveInput, param) = fill(1., Int(length(param)/3))
# @inline initial_x(::FixedRateCascadeModel, ::AbstractInput, param) = fill(1.,round(Int, param[2]-1))
@inline initial_x(::DDEModel, ::WaveInput, param) = [param[1]]
@inline initial_x(::GammaDDEModel, ::WaveInput, param) = [param[1]]
# @inline initial_x(::FixedRateHillModel, ::WaveInput, param) = vcat(fill(1., round(Int, param[2]-1)), param[1])


@inline get_h(::AbstractDDEModel, ::AbstractInput, ::Any) = (p, t; idxs=nothing) -> idxs isa Number ? 0. : [0., 0.]
@inline get_h(::AbstractDDEModel, ::WaveInput, ::Any) = (p, t; idxs=nothing) -> idxs isa Number ? 1. : [1., 1.]


"""
    get_callback!(model, input)

Returns:
- A callback
- A vector of tstops for the solver.
"""
@inline get_callback(::AbstractModel, ::AbstractInput, param) = (nothing, [])
function get_callback(::AbstractModel, ::PiecewiseInput, param)
    function cond(u, t, i)
        t == 100
    end
    
    function affect!(i)
        i.u[1] = 0.
    end
    return (DiscreteCallback(cond, affect!), [100.])
end

function get_callback(::AbstractModel, input::PulseInput, param)
    function cond(u, t, i)
        t == input.duration
    end
    
    function affect!(i)
        i.u[1] = 0.
    end
    return (DiscreteCallback(cond, affect!), [input.duration])
end

function get_callback(::AbstractDDEModel, ::ImpulseInput, param)
    function cond(u, t, i)
        t == param[2]
    end
    
    function affect!(i)
        i.u[2] += param[3] * param[1]
    end
    return (DiscreteCallback(cond, affect!), [param[2]])
end

################################################################################
############################  Simulation function  #############################
################################################################################

get_de_problem(model::AbstractODEModel, input, u0, tspan, p) = ODEProblem(build_de(model, input), u0, tspan, p) 

get_de_problem(model::GammaDDEModel, input, u0, tspan, p) = DDEProblem{true}(build_de(model, input), u0, get_h(model, input, p), tspan, p) 

get_de_problem(model::DDEModel, input, u0, tspan, p) = DDEProblem{true}(build_de(model, input), u0, get_h(model, input, p), tspan, p; constant_lags=[p[2]]) 

function simulate(model::AbstractDEModel, input::AbstractInput, param; tstop=5000., alg=AutoTsit5(Rosenbrock23()), save_vars = :first_last, callback=nothing, kwargs...)
    u0 = get_u0(model, input, param)
    du = copy(u0)
    tspan = (0., tstop)
    parameters = copy(param)
    default_callback, tstops = get_callback(model, input, parameters)
    callbackset = CallbackSet(callback, default_callback)

    if save_vars == :first_last
        save_idxs = [1, length(u0)]
    elseif save_vars == :not_first
        save_idxs = collect(2:length(u0))
    elseif save_vars == :last
        save_idxs = [length(u0)]
    elseif save_vars == :first
        save_idxs = [1]
    elseif save_vars == :all
        save_idxs = nothing
    else
        error("invalid `save_idxs` value to `simulate`. Valid options are :first_last, :first, :last and :all")
    end
    
    prob = get_de_problem(model, input, u0, tspan, parameters)
    if model isa GammaDDEModel
        sol = solve(
            prob,
            MethodOfSteps(RK4()); 
            save_idxs=save_idxs,
            callback=callbackset,
            tstops=tstops,
            kwargs...,
        )
    else
        sol = solve(
            prob,
            alg;
            save_idxs=save_idxs,
            callback=callbackset,
            tstops=tstops,
            kwargs...,
        )
    end
    return sol
end

function simulate(model::FixedRateModel, input::ImpulseInput, param; kwargs...)
	return t -> gamma_model(t, param[1], round(Int, param[2]), param[3])
end


function simulate(::Union{GammaModel, GammaHillModel}, ::AbstractInput, args...; kwargs...)
	@error "The small, functional, gamma models GammaModel and GammaHillModel only works for ImpulseInput"
end

# This is similar to the FixedRateModel, but it allows non-integer values of 
# the n-parameter. 
function simulate(::GammaModel, ::ImpulseInput, params; kwargs...)
    return t -> gamma_model(t, params...)
end

function simulate(::GammaHillModel, ::ImpulseInput, params; kwargs...)
    return t -> gamma_hill(t, params...)
end

function simulate(::GammaGeneralModel, input, params; kwargs...)
    return t -> gamma_general(input, params, t)
end

simulate(model::GammaGeneralModel, input::ImpulseInput, params; kwargs...) = simulate(GammaModel(), input, params; kwargs...)

################################################################################
########################  Simulation with noisy input  #########################
################################################################################

prod_rate(u,p,t) = (1 + 9 * u[1] / (5 + u[1]))/10
prod_affect!(i) = (i.u[1] += 1)
prod_jump = ConstantRateJump(prod_rate, prod_affect!)

deg_rate(u,p,t) = u[1]/10
deg_affect!(i) = (i.u[1] -= 1)
deg_jump = ConstantRateJump(deg_rate, deg_affect!)

function simulate(model::AbstractDEModel, input::NoiseInput, param; tstop=100, callback=nothing, save_vars=:first_last, kwargs...)
	u0 = get_u0(model, input, param)
    parameters = copy(param)

	du = copy(u0)
	tspan = (0., tstop) 

    if save_vars == :first_last
        save_idxs = [1, length(u0)]
    elseif save_vars == :not_first
        save_idxs = collect(2:length(u0))
    elseif save_vars == :last
        save_idxs = [length(u0)]
    elseif save_vars == :first
        save_idxs = [1]
    elseif save_vars == :all
        save_idxs = nothing
    else
        error("invalid `save_idxs` value to `simulate`. Valid options are :first_last, :first, :last and :all")
    end

	prob = get_de_problem(model, input, u0, tspan, parameters)
	jump_prob = JumpProblem(prob, Direct(), prod_jump, deg_jump)
    if model isa GammaDDEModel
        sol = solve(jump_prob, MethodOfSteps(RK4()); kwargs...)
    else
        sol = solve(jump_prob; save_idxs=[1, length(u0)], callback=callback, kwargs...)
    end
end

function simulate(model::AbstractDEModel, input::RepeatedNoiseInput, param; scale_input = 1, save_vars=:first_last, alg=AutoTsit5(Rosenbrock23()), tstop=100, callback=nothing, kwargs...)

    pos_cond(u,t,i) = t in input.positive_jump_times
	pos_affect!(i) = (i.u[1] += scale_input)

	neg_cond(u,t,i) = t in input.negative_jump_times
	neg_affect!(i) = (i.u[1] -= scale_input)

	pos_cb = DiscreteCallback(pos_cond, pos_affect!)
	neg_cb = DiscreteCallback(neg_cond, neg_affect!)

	callback_set = CallbackSet(pos_cb, neg_cb)


	u0 = get_u0(model, input, param)
    parameters = copy(param)
    
    if save_vars == :first_last
        save_idxs = [1, length(u0)]
    elseif save_vars == :not_first
        save_idxs = collect(2:length(u0))
    elseif save_vars == :last
        save_idxs = [length(u0)]
    elseif save_vars == :first
        save_idxs = [1]
    elseif save_vars == :all
        save_idxs = nothing
    else
        error("invalid `save_idxs` value to `simulate`. Valid options are :first_last, :first, :last and :all")
    end

	du = copy(u0)
	tspan = (0., tstop) 
	prob = get_de_problem(model, input, u0, tspan, parameters)
    sol = solve(prob,
                alg; 
                save_idxs=save_idxs,
                callback = callback_set, 
                tstops=[input.positive_jump_times; input.negative_jump_times],
                kwargs...
               )
end


################################################################################
#################  Utilities for working with noise solutions  #################
################################################################################

function find_positive_steps(sol) 
	u1 = hcat(sol.u...)[1,:]
	jumps = u1[2:end] - u1[1:end-1]
	return sol.t[findall(x -> x == 1, jumps)]
end

function find_negative_steps(sol) 
	u1 = hcat(sol.u...)[1,:]
	jumps = u1[2:end] - u1[1:end-1]
	return sol.t[findall(x -> x == -1, jumps)]
end

