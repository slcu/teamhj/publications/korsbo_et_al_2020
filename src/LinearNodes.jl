module LinearNodes

using Reexport
@reexport using DiffEqParameters
import CostFunctions
using BlackBoxOptim
using Dierckx
using DifferentialEquations
using Interpolations
using LaTeXStrings
using Optim
using Plots
using QuadGK
using RecipesBase
using SpecialFunctions: gamma
using Statistics
using Measures


include("models.jl")
export simulate

export CascadeModel, DDEModel, FixedRateCascadeModel, FixedRateHillModel, FixedRateMMModel,
    FixedRateModel, FixedStepModel, GammaDDEModel, GammaGeneralModel, GammaHillModel,
    GammaModel, MMChainModel, AbstractModel

export AbstractInput, AbstractNoiseInput, DecayingInput, ImpulseInput,
    NoiseInput, PiecewiseInput, PulseInput, RampInput, RepeatedNoiseInput, StepInput,
    WaveInput

export find_negative_steps, find_positive_steps

struct ResultGeneral{T <: AbstractInput, T2 <: AbstractFunctionalModel} <: AbstractParameter{Float64}
	model::FixedStepModel
    param::Vector{Float64}
	input::T
    one_node_param::Vector{Float64}
    one_node_cost::Float64
    two_node_param::Vector{Float64}
    two_node_cost::Float64
    three_node_param::Vector{Float64}
    three_node_cost::Float64
    four_node_param::Vector{Float64}
    four_node_cost::Float64
    five_node_param::Vector{Float64}
    five_node_cost::Float64
    fixed_rate_model::FixedRateModel
    fixed_rate_param::Vector{Float64}
    fixed_rate_cost::Float64
    gamma_model::T2
    gamma_param::Vector{Float64}
    gamma_cost::Float64
    dde_model::DDEModel
    dde_param::Vector{Float64}
    dde_cost::Float64
    tstop::Float64
end


struct Result{T <: AbstractModel, T2 <: AbstractInput} <: AbstractParameter{Float64}
	model::T
    param::Vector{Float64}
    inputs::Vector{T2}
    fitted_models::Dict{Symbol, Tuple}
    tstop::Float64
end

struct ResultMulti{T <: AbstractModel, T2 <: AbstractInput} <: AbstractParameter{Float64}
	model::T
    param::Vector{Float64}
    inputs::Vector{T2}
    fitted_models::Dict{Symbol, Tuple}
    tstops::Vector{Float64}
end


# import Base: getindex
# Base.getindex(p::Result, model::AbstractModel) = p.fitted_models[model]


export ResultGeneral, Result, ResultMulti

include("plot_recipes.jl")
include("utils.jl")

export data, cost, param, get_model, node_cost, node_param, macost, maparam, t_mean,
    t_std, t_var, r_mean, r_std, sharpness, rate_homogeneity, get_tstops

include("optimise.jl")
export opt, opt_homogeneous, reoptimise, optimise, opt_multi

include("analysis.jl")
export saturation, evaluate_cost, saturation_summary, saturation_integral

include("analytic_parameter_estimation.jl")
export estimate_mean, estimate_variance, estimate_param, estimate_n, estimate_r, 
    estimate_γ, PDF, CDF, pdf2cdf

end # module
