interp(t, y) = Spline1D(t, y)

#==============================================================================#
#=======================  Convert PDF data to CDF data  =======================# 
#==============================================================================#

function pdf2cdf(t, y)
    y_interp = interp(t, y)
    y_cum = [quadgk(t -> y_interp(t), t[1], time)[1] for time in t]
    return (t, y_cum)
end

#==============================================================================#
#=====  Estimate mean and variance from points tracking the PDF or CDF.  ======# 
#==============================================================================#

abstract type IsInterpolated end
struct Interpolated <: IsInterpolated end
struct NotInterpolated <: IsInterpolated end

isinterpolated(::Type) = NotInterpolated()
isinterpolated(::Type{Spline1D}) = Interpolated()
isinterpolated(::Type{<:AbstractInterpolation}) = Interpolated()

abstract type DataKind end
struct PDF <: DataKind end
struct CDF <: DataKind end

#===================================  Mean  ===================================# 

estimate_mean(datatype::DataKind, t, y::T) where T = estimate_mean(isinterpolated(T), datatype, t, y)
estimate_mean(::NotInterpolated, datatype::DataKind, t, y) = estimate_mean(datatype, t, interp(t, y))

function estimate_mean(::Interpolated, datatype::CDF, t, y) 
    h = 1e-3
    quadgk(x -> x * (y(x) - y(x - h))/h, t[1]+h, t[end])[1] / y(t[end])
    # quadgk(x -> 1 - y(x)/y(t[end]), t[1], t[end])[1]
end

function estimate_mean(::Interpolated, datatype::PDF, t, y) 
    quadgk(x -> x * y(x), t[1], t[end])[1] / quadgk(t -> y(t), first(t), last(t))[1]
end

#=================================  Variance  =================================# 

estimate_variance(datatype::DataKind, t, y::T) where T = estimate_variance(isinterpolated(T), datatype, t, y)
estimate_variance(::NotInterpolated, datatype::DataKind, t, y) = estimate_variance(datatype, t, interp(t, y))

function estimate_variance(::Interpolated, datatype::CDF, t, y) 
    t_mean = estimate_mean(datatype, t, y)

    y_asymptote = mean(map(y, t[round(Int, 0.8 * end):end]))
    t2_mean, err = quadgk(x -> 2 * x * (1 - y(x)/y_asymptote), t[1], t[end]) 

    t2_mean - t_mean^2
end

function estimate_variance(::Interpolated, datatype::PDF, t, y) 
    t_mean = estimate_mean(datatype, t, y)
    quadgk(t -> (t - t_mean)^2 * y(t), t[1], t[end])[1] / quadgk(t -> y(t), t[1], t[end])[1]
end


#==============================================================================#
#=========  Estimate params using data mean and var for PDF and CDF  ==========# 
#==============================================================================#

estimate_γ(::PDF, t, y) = sum((y[2:end] .+ y[1:(end - 1)]) ./ 2 .* (t[2:end] .- t[1:(end - 1)]))
estimate_γ(::CDF, t, y) = y[end]
estimate_r(datakind::DataKind, t, y) = estimate_mean(datakind, t, y) / estimate_variance(datakind, t, y)
estimate_n(datakind::DataKind, t, y) = estimate_mean(datakind, t, y)^2 / estimate_variance(datakind, t, y)
estimate_param(datakind::DataKind, t, y) = (estimate_γ(datakind, t, y), estimate_n(datakind, t, y), estimate_r(datakind, t, y))


#==============================================================================#
#=======================  Support for manual testing.  ========================# 
#==============================================================================#


"""
    runtest(t, y)

test the estimation for the PDF and CDF method.

y must be PDF-style data.
"""

function runtest(t, y)
    @show pdf_mean = estimate_mean(PDF(), t, y)
    @show cdf_mean = estimate_mean(CDF(), pdf2cdf(t, y)...)
#
    println()
    @show pdf_var = estimate_variance(PDF(), t, y)
    @show cdf_var = estimate_variance(CDF(), pdf2cdf(t, y)...)
#
    println()
    cγ = pdf2cdf(t, y)[end][end]

    cn = cdf_mean^2 / cdf_var
    cr = cdf_mean / cdf_var

    @show (cγ, cn, cr)
    scatter(pdf2cdf(t, y); dpi = 200);
    sim = simulate(GammaGeneralModel(), StepInput(), [cγ, cn, cr])
    plot!(sim, range(first(t), stop = last(t), length=300); legend=:bottomright, label="CDF estimate")
    sim = simulate(GammaGeneralModel(), StepInput(), estimate_param(PDF(), t, y))
    plot!(sim, range(first(t), stop = last(t), length=300), label="PDF estimate")
end
