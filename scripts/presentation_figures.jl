using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)
Pkg.instantiate()

isdir(joinpath(root, "results")) || mkdir(joinpath(root, "results"))

using LinearNodes
using Plots
using PGFPlotsX
using LaTeXStrings
using FileIO
using JLD2
using Measures
using Latexify
path = split(@__DIR__, "scripts")[1]

savepng(fname) = savepng(plot!(), fname)
function savepng(plt, fname)
    savefig(plt, fname * ".pdf")
    PGFPlotsX.convert_pdf_to_png(fname * ".pdf", fname; dpi=600)
end

bg_color = RGB(((250, 250, 250) ./ 255)...)
Plots.default(; bg_color, legend=:topright)
savepath = "$root/presentations/siam2021/images/"
#==============================================================================#
#==========  Define parameters for the plotting and load datasets.  ===========# 
#==============================================================================#
pgfplotsx()

pc = load("$path/parameters/ResultGeneral.jld2", "pc")
pc_homogeneous = load("parameters/ResultGeneralHomogeneous.jld2", "pc")

p_noise = @filter pc input isa AbstractNoiseInput
p_step = @filter pc input isa StepInput
p_piecewise = @filter pc input isa PiecewiseInput
p_wave = @filter pc input isa WaveInput
p_impulse = @filter pc input isa ImpulseInput
p_ramp = @filter(pc, input isa RampInput && two_node_cost < 10)


#==============================================================================#
## demo the issue in a single plot
pf = @filter pc input isa ImpulseInput && length(param) == 11
_, idx = findmax(cost.(pf, :two_node))
plot(pf[idx]; plotmodels=[:two_node, :data], legend=:topright, size=(300,200), dpi=600, bg_color, label=["10-step \"truth\"" "2-step fitted"], yguide=L"X_n")
savefig(savepath * "two_step.pdf")


plot(; layout=(2,3));
for (i, dl) in enumerate([2,3,5,10,25,50])
    pf = @filter pc input isa ImpulseInput && length(param) == dl
    _, idx = findmax(cost.(pf, :two_node))
    plot!(subplot=i, pf[idx]; plotmodels=[:two_node, :data], legend=:topright, dpi=600, bg_color, label=["$(dl)-step \"truth\"" "2-step fitted"])
end
plot!()

costdisplay(p_impulse, :two_node)


##
pgfplotsx()

# Plots.resetfontsizes()
# Plots.scalefontsizes(plt_scaling)

_save = true

plt_kws = (;plotmodels=[:two_node, :data], framestyle=:axes, legend=false, yguide="Conc.")

counter = 1
figname = "two_node_cost"
fmt = "png"
l = @layout [aw th; a{0.4h} ; b c]
plt = plot(layout=l, framestyle=:none);
pathwaylengthcost!(subplot=3, p_impulse, :two_node, framestyle=:axes);
# plot!()
fpath = "$savepath/$(figname)_$(counter)"
_save && savefig(fpath * ".pdf")
_save && PGFPlotsX.convert_pdf_to_png(fpath * ".pdf", fpath; dpi=600)
counter +=1

p = @filter(p_impulse, length(param)==3)[5]
plot!(subplot=1, p; xlims=(0., 200.), plt_kws..., legend=:topright);
scatter!(subplot=3, [2], [cost(p, :two_node)]; ms=7, color=2, legend=false)
fpath = "$savepath/$(figname)_$(counter)"
_save && savefig(fpath * ".pdf")
_save && PGFPlotsX.convert_pdf_to_png(fpath * ".pdf", fpath; dpi=600)
counter +=1

p = @filter(p_impulse, length(param)==51)[5]
plot!(subplot=2, p; plt_kws...);
scatter!(subplot=3, [50], [cost(p, :two_node)]; ms=7, color=2, legend=false)
fpath = "$savepath/$(figname)_$(counter)"
_save && savefig(fpath * ".pdf")
_save && PGFPlotsX.convert_pdf_to_png(fpath * ".pdf", fpath; dpi=600)
counter +=1

pf = @filter(p_impulse, length(param)==21)
p = pf[findmin(cost.(pf, :two_node))[2]]
plot!(subplot=4, p; plt_kws...);
scatter!(subplot=3, [20], [cost(p, :two_node)]; ms=7, color=2, legend=false)
fpath = "$savepath/$(figname)_$(counter)"
_save && savefig(fpath * ".pdf")
_save && PGFPlotsX.convert_pdf_to_png(fpath * ".pdf", fpath; dpi=600)
counter +=1

pf = @filter(p_impulse, length(param)==21)
p = pf[findmax(cost.(pf, :two_node))[2]]
plot!(subplot=5, p; plt_kws...);
scatter!(subplot=3, [20], [cost(p, :two_node)]; ms=7, color=2, legend=false)
fpath = "$savepath/$(figname)_$(counter)"
_save && savefig(fpath * ".pdf")
_save && PGFPlotsX.convert_pdf_to_png(fpath * ".pdf", fpath; dpi=600)
counter +=1

# plt = plot!()
# pgfsave("$savepath/$(figname)_$(counter)_pgf_test.$fmt", plot!().o.the_plot, dpi=600)
# pgfsave("test.png", plot!().o.the_plot, dpi=600)

plot!()
##

pgfplotsx()
n = 4
r = 1
sim = simulate(FixedRateModel(), ImpulseInput(), [1., n, r])
plot(size=(250,200), grid=false)
plot!(range(n/r - sqrt(n/r^2), stop= n/r + sqrt(n/r^2), length=300), sim, fillrange=(0.), color=:black, alpha=0.6, lw=5, linealpha=0, fillalpha=0.4, fillcolor=2, label="")
plot!(range(0., stop=10., length=300), sim, lw=6, color=:grey, label="")
plot!([n/r, n/r], [0., sim(n/r)], color=1, alpha=0.6, lw=5, label="Mean")
plot!([n/r - sqrt(n/r^2), n/r + sqrt(n/r^2)], sim(n/r) .* 0.5 .* [1, 1], color=2, alpha=0.6, lw=5, label="Std")
xticks!([n/r, (n/r - sqrt(n/r^2)),  (n/r + sqrt(n/r^2))], latexify.([:τ, :(τ - σ), :(τ + σ)]))
yticks!(Float64[],String[])
plot!(; xguide = "Time", yguide = L"X_n(t)")

savepng(savepath * "/mean_std")
plot!()

##
pgfplotsx()

plt_kws = (; size=(300, 250), grid=false, color=[:black 1 :black :black])
sharpnessplot(p_impulse; plotmodels=[], plt_kws..., color=[:black :black])
savepng(savepath * "/sharpness_1")
sharpnessplot(p_impulse; plotmodels=[:data], plt_kws..., color=[:black :black :black])
savepng(savepath * "/sharpness_2")
sharpnessplot(p_impulse; plotmodels=[:data, 2], plt_kws..., color=[:black 1 :black :black])
savepng(savepath * "/sharpness_3")

# savepng(savepath * "/sharpness")

#======================  rate homogeneity vs sharpness  =======================#
##
pgfplotsx()
subplot = 1
ms = 5

scatter(
    subplot = subplot,
    sharpness.(p_impulse),
    rate_homogeneity.(p_impulse),
    alpha = 0.1,
    markercolor = :black,
    label = "Data",
    markerstrokealpha = 0.1,
    # size = (500,250),
    size = (300,250),
    markersize = ms,
    grid=false,
);
for i in [5, 2]
    pc_longer = @filter(p_impulse, length(param) - 1 >= i)
    scatter!(
        subplot = subplot,
        sharpness.(pc_longer),
        rate_homogeneity.(pc_longer, i),
        markercolor = i == 2 ? 1 : 5,
        alpha = 0.3,
        label = "$i-step model",
        markerstrokealpha = 0.1,
        markersize = ms,
    )
end
xticks!(
    subplot = subplot,
    [1:4; [sqrt(i) for i in [2, 5]]],
    LaTeXString.(string.([1:4; ["\$\$\\sqrt{$i}\$\$" for i in [2, 5]]])),
);
plot!(
    subplot = subplot,
    xlabel = L"\textrm{Data sharpness, }s_{data}",
    # ylabel = L"\textrm{Rate dispersion, }\sigma_r/\langle r \rangle",
    ylabel = "Rate dispersion",
    legend = :topright,
    ylims = (-0.05, 3.5),
    yticks = 0:3,
)

savepng(savepath * "/sharpness_dispersion")
##

pgfplotsx()
plot(p_impulse[1]; plotmodels=[:data, :fixed_rate, :two_node], size=(300,200), plotcolors=Dict(:two_node=>1), label=["Data" "Erlang" "Two-step"])
savepng(savepath * "/fixed_rate_demo")


##
pgfplotsx()

pathwaylengthcost(p_impulse, :fixed_rate; comparison=:two_node, size=(300,200), grid=false, color=[1 2])
savepng(savepath * "/fixed_rate_cost")

##
# multiinputplot(pc)

pgfplotsx()
plt_kws = (; plotmodels=[:input, :data, :fixed_rate, :two_node], legend=false, plotcolors=Dict(:fixed_rate=>2, :two_node=>1), label=["Input" "Data" "Erlang" "Two-step"])
l = @layout [a b c{0.33w}]
plot(layout=l, size=(600,200), extra_kwargs=Dict(:subplot=>Dict("legend columns"=>"-1", "legend style"=>"{at={(0.2, -0.35)}, /tikz/every even column/.append style={column sep=0.5cm}}")))
plot!(subplot=1, p_impulse[1]; plt_kws...)
plot!(subplot=2, p_step[1]; plt_kws...)
plot!(subplot=3, p_noise[22]; plt_kws..., legend=true)
##

savepng(savepath * "/fixed_rate_multi_input")
##

