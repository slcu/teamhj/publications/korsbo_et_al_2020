using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

# using BlackBoxOptim
using DataFrames
using LinearNodes
using FileIO
using CSVFiles
using Plots
Plots.default(grid=false)
root_dir = split(pwd(), "scripts")[1]


struct Data
    t::Vector{Float64}
    y::Vector{Float64}
    name::String
    author::String
    year::Int
end

function Data(file_path::String, args...)
    values = DataFrame(load(file_path, colnames = [:t, :y], header_exists = false))
    Data(values[:t], values[:y], args...)
end



#################################################
# Monaghan et al. 2014; The calcium-dependent ...
#################################################

bio_data = Dict{String,Data}()
elicitors = ["flg22", "elf18", "AtPep1"]
for (i, elicitor) in enumerate(elicitors)
    input = joinpath(
        root_dir,
        "data",
        "monaghan2014",
        "figure3",
        "fig_3_$(('a':'d')[i])_$elicitor.csv",
    )
    bio_data["monaghan_3$(('a':'c')[i])_$elicitor"] =
        Data(input, "$elicitor - ROS", "Monaghan et al.", 2014)
end
d = bio_data["monaghan_3a_flg22"]
d = bio_data["monaghan_3c_AtPep1"]

#==============================================================================#
#===================  Optimise the models towards the data  ===================# 
#==============================================================================#

resultfile = joinpath(root, "parameters", "experimental_data_params.jld2")
if isfile(resultfile) 
    results = load(resultfile)
else
    ## This takes about a lunch break to finish.
    results = Dict{String,ResultGeneral}()
    for i in eachindex(elicitors)
        key = "monaghan_3$(('a':'d')[i])_$(elicitors[i])"
        results[key] = opt(
            ImpulseInput(),
            Float64[],
            bio_data[key].t,
            bio_data[key].y;
            tstop = 45.0,
            silent = false,
        )
    end
    save(resultfile, result)
end

#==============================================================================#
#===================================  Plot  ===================================# 
#==============================================================================#

pgfplotsx()

plt = plot(layout = (1,3), size = (600, 150), legend = false);
for i in eachindex(elicitors)
    key = "monaghan_3$(('a':'d')[i])_$(elicitors[i])"
    scatter!(subplot = i, bio_data[key].t, bio_data[key].y, ms = 2, color = :black, label = "Data")
    plot!(
        subplot = i,
        simulate(
            FixedRateModel(),
            ImpulseInput(),
            results[key].fixed_rate_param;
            tstop = 45.0,
        ),
        range(0.0, stop = 45, length = 300),
        linewidth = 3,
        color = 1,
        label = "Fixed-rate",
        alpha = 0.85,
    )
    plot!(
        subplot = i,
        simulate(
            FixedStepModel(),
            ImpulseInput(),
            results[key].two_node_param;
            tstop = 45.0,
        ),
        vars = 2,
        linewidth = 3,
        color = 2,
        label = "Two-step",
        alpha = 0.85,
    )
    plot!(
        subplot = i,
        xlabel = "Time [minutes]",
        # title = "$(('A':'Z')[i])) $(elicitors[i])",
        title = "$(('A':'Z')[i])",
    )
end
plot!(subplot=3, legend=:outerright);
plot!(subplot = 1, ylabel = "ROS [RLU]");
plot!(grid = false, dpi = 600)


figname = "immune_example"
foreach(fmt -> savefig("results/$(figname).$fmt"), [:pdf, :png])

