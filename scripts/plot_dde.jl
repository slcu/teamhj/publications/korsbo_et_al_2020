using Pkg
path = string(split(@__DIR__, "scripts")[1])
Pkg.activate(path)

isdir(joinpath(root, "results")) || mkdir(joinpath(root, "results"))

using LinearNodes
using FileIO
using JLD2
using Plots


pc = load("$path/parameters/ResultGeneral.jld2", "pc")

pgfplotsx()

ddeplot(
    pc,
    cost_scale = :log10,
    markeralpha = 0.01,
    aspect_ratio = :none,
    comparison1 = :fixed_rate,
    comparison2 = :two_node,
    linestyle = :solid,
    size = (700, 500),
    title = permutedims(["$fig" for fig in 'A':'Z']),
    plotcolors = Dict(:fixed_rate=>1, :two_node=>2, :dde=>3, :data=>:black, :input=>:grey),
    title_location = (0.5, 0.9),
    legendfonthalign=:left,
    foreground_color_legend = :transparent,
    background_color_legend = :transparent,
);
plot!(subplot=2, legend=(.01, 1.27), legendfontsize=9, );
map(subplot -> LinearNodes.reset_yticks!(plot!(), subplot), 2:3:9);

## Move an annotation that is covered by data-dots
ann = plot!().subplots[7].attr[:annotations]
ann[1] = (ann[1][1] - .6, ann[1][2] -.6, ann[1][3])
plot!(subplot=7, ann = ann);

plot!(dpi = 600);
for format in [:png, :pdf]
    savefig("$path/results/dde_plot.$format")
end
