using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using Plots
using FileIO
using JLD2
using LaTeXStrings
using Measures

gr()

pc = load(joinpath(root, "parameters", "ResultGeneral.jld2"), "pc")

pc_none = @filter(pc, input isa ImpulseInput)
pc_noise = @filter(pc, input isa AbstractNoiseInput)


# plt = plot(layout=(2,2), grid=false, size=(600,350), fontfamily="times", titlelocation=(0.5, 0.9));
plt = plot(layout=(2,2), grid=false, size=(600,350), titlelocation=(0.5, 0.9));
for (i, pc_filtered) in enumerate([pc_none, pc_noise])
    p_median = LinearNodes.geometric_median(pc_filtered, :gamma_cost, :fixed_rate_cost)
    scatter!(
        subplot = 2*(i-1) + 1, 
        log10.(pc_filtered[:gamma_cost]), 
        log10.(pc_filtered[:fixed_rate_cost]), 
        #         zcolor=hcat(pc_filtered[:gamma_param]...)[2,:],
        zcolor=length.(pc_filtered[:param]) .- 1,
        markerstrokealpha=0,
        markeralpha=0.3,
        label="", 
        grid=false, 
        zlabel=L"n_{data}", 
        # zlabel="n_{data}", 
        ylabel=L"\textrm{Fixed-rate cost, }\log_{10}", 
        # ylabel="Fixed-rate cost, log₁₀", 
        # xlabel="Gamma cost, log₁₀", 
        xlabel=L"\textrm{Gamma cost, }\log_{10}", 
        #         clims=(1, 30),
        colorbar_title=L"n_{data}",
        # title = latexstring("\\textrm{$(['A', 'C'][i])}"),
        title = "$(['A', 'C'][i])",
        #     scale=:log10,
        right_margin=6mm,
    );
    scatter!(
        subplot = 2*(i-1) + 1, 
        log10.([p_median.gamma_cost]), 
        log10.([p_median.fixed_rate_cost]),
        color=3,
        ms = 6,
        label="",
    );
    plot!(subplot = 2*(i-1) + 2, [0],[0], lw=1, color=:grey, label = "Input")
    plot!(
        subplot = 2*(i-1) + 2, 
        p_median;
        xlabel = L"\textrm{Time}",
        ylabel = L"\textrm{Concentration}",
        # title = latexstring("\\textrm{$(['B', 'D'][i])}"),
        title = "$(['B', 'D'][i])",
        legend = i == 1 ? (0.6,0.8) : false,
        plotmodels= i == 1 ? [:data, :fixed_rate, :gamma] : [:input, :data, :fixed_rate, :gamma],
        linestyle = i == 1 ? [:solid :solid :dash] : [:solid :solid :solid :dash],
        linecolor = i == 1 ? [:black 1 2] : [:grey :black 1 2],
        # label = [latexstring("\\textrm{$str}") for str in ["Input", "Data", "Fixed-rate", "Gamma"]],
        label = ["Data" "Fixed-rate" "Gamma"],
        linealpha=1,
    );
end
# plot!([],[], subplot=2, legend=true, color=[:grey :black 1 2], lw = [1 4 2 2], label = ["Input" "Data" "Fixed-rate" "Gamma"],)
# plot!(subplot=2, top_margin=0mm, titlelocation=(0.5, 0.3));

plot!(dpi = 600);
for fmt in [:pdf, :png]
    savefig(joinpath(root, "results", "gamma_vs_fixed_rate_$(Plots.backend_name()).$fmt"))
end

pgfplotsx()

