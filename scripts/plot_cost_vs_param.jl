using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using FileIO
using Plots
using Latexify
pgfplotsx()


pc = load("parameters/ResultGeneral.jld2", "pc")

for input_type in [ImpulseInput, RampInput, StepInput, RepeatedNoiseInput, WaveInput, PiecewiseInput]
    pc_filtered = @filter(pc, input isa input_type)
    param_names = Dict(
                       :two_node => (:γ, :r_1, :r_2),
                       :fixed_rate => (:γ, :n, :r),
                       :gamma => (:γ, :n, :r),
                       :dde => (:γ, :τ, :r),
                      )
    plot(layout = (4,3), size=(600, 600), legend=false, grid=false);
    for (i, model) in enumerate([:two_node, :fixed_rate, :gamma, :dde])
        param = hcat(pc_filtered[Symbol(model, :_param)]...)
        for j in 1:3
            subplot = 3*i-3 + j
            ylabel=uppercasefirst("$(replace(replace(string(model), '_'=>' '), "node"=>"step")) cost")
            model == :dde && (ylabel = "Fixed delay cost")
            j == 1 && plot!(subplot = subplot, ylabel=ylabel);
            scatter!(
                     subplot=subplot,
                     log10.(param[j,:]),
            pc_filtered[Symbol(model, :_cost)],
            alpha=0.05,
            xlabel = latexify(:(log10($(param_names[model][j]))))
           );
        end
    end
    savefig("results/param_vs_cost_$(input_type).svg")
    savefig("results/param_vs_cost_$(input_type).pdf")
    savefig("results/param_vs_cost_$(input_type).png")
end
println("done!")

