using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using Plots
using FileIO
using JLD2
using Measures
using LaTeXStrings
using Distributed

model_name = :fixed_rate
costfield = Symbol("$(model_name)_cost")
paramfield = Symbol("$(model_name)_param")

pgfplotsx()
plot(x->x)

pc = load("parameters/ResultGeneral.jld2", "pc")

spread_param = load("$root/parameters/ResultGeneralSpread.jld2", "pc")

overwrite = false
if isfile("parameters/n_data_vs_inhomogeniety.jld2") && !overwrite
    pc_hetro = load("parameters/n_data_vs_inhomogeniety.jld2")
    pc_hetro_f = pc_hetro["fixed_rate"]
    pc_hetro_2 = pc_hetro["two_step"]

    δrange = getfield.(pc_hetro_f[1,:], :δ)
    nrange = getfield.(pc_hetro_f[:,1], :n)

    @assert getfield.(pc_hetro_2[1,:], :δ) == δrange
    @assert getfield.(pc_hetro_2[:,1], :n) == nrange
else 
    nprocs() == 1 && addprocs(8)
    @everywhere using Pkg
    @everywhere Pkg.activate(root)
    @everywhere using LinearNodes
    #
    nrange = 1:25
    δrange = 0:0.1:3
    M = [(n, δ) for n in nrange, δ in δrange]
    #
    ## Force compilation
    @everywhere LinearNodes.opt_heterogeneous(FixedRateModel(); n=1, δ=0.)
    @everywhere LinearNodes.opt_heterogeneous(FixedStepModel(); n=2, δ=0.)
    #
    ## This one is pretty quick
    @time pc_hetro_f = pmap(x -> LinearNodes.opt_heterogeneous(FixedRateModel(); n=x[1], δ=x[2]), M)
    # @time pc_hetro_f = map(x -> LinearNodes.opt_heterogeneous(FixedRateModel(); n=x[1], δ=x[2]), M)
    #
    ## This one takes about 4 minutes using 8 threads
    @time pc_hetro_2 = pmap(x -> LinearNodes.opt_heterogeneous(FixedStepModel(); n=x[1], δ=x[2]), M)
    #
    save("parameters/n_data_vs_inhomogeniety.jld2", "fixed_rate", pc_hetro_f, "two_step", pc_hetro_2)
end


#<<
l = @layout [a{0.45h}; grid(1,5){0.25h}; grid(1,3){0.3h}]
plt = plot(
    layout=l,
    grid=false,
    size=(600,500),
);


#==============================================================================#
#========================  Cost vs inhomogeniety plot  ========================# 
#==============================================================================#
for (i, nodes) in enumerate([:one, :two, :three, :four, :five])
    plot!(
        subplot=1,
        range(0., stop=3, length=22),
        spread_param[Symbol("$(nodes)_node_cost")]; 
        label=uppercasefirst("$nodes-step"),
        lw= i==2 ? 6 : 2,
        color = i == 1 ? 6 : i,
        alpha=0.6,
        ms = 3,
    );
    scatter!(
        subplot=1,
        range(0., stop=3, length=22),
        color = i == 1 ? 6 : i,
        spread_param[Symbol("$(nodes)_node_cost")]; 
        primary=false
    );
end


plot!(
    subplot=1,
    range(0., stop=3, length=22),
    spread_param[costfield]; label="Fixed-rate",
    lw=6,
    alpha=0.6,
    color = 1,
    primary=true
);
scatter!(
    subplot=1,
    range(0., stop=3, length=22),
    color = 1,
    spread_param[costfield],
    primary=false,
);
plot!(
    subplot=1,
    xlabel="Data transfer rate inhomogeniety, \\delta",
    ylabel="Optimised model cost",
    ylims=(-0.1,1.),
    legend=:topright,
);


#==============================================================================#
#==========  Sample trajectories and highlights in the scatter plot  ==========# 
#==============================================================================#
for (j, i) in enumerate(1:7:22)
    plot!(
        subplot = j+1,
        spread_param[i],
        #= color=["black" 2 6], =#
        plotmodels=[:data, model_name, :two_node],
        plotcolors=Dict(:data=>:black, model_name=>1, :two_node=>2),
        ms=1,
        #= lw=4, =#
        #= mastyle=:solid, =#
        tstop_factor=2/3,
        #= linealpha=0.6, =#
        legend=false
    );
    j > 1 && plot!(subplot=j+1, ylabel="");
    scatter!(
        subplot = 1,
        [j-1],
        [getfield(spread_param[i], costfield)],
        color=1,
        ms = 7,
        primary=false
    );

    y_annotation = i == 1 ? 0.10 : 0.15
    scatter!(
        subplot = 1,
        [j-1],
        [spread_param[i].two_node_cost],
        color = 2,
        ms = 7,
        primary=false,
        annotate=(j-1,spread_param[i].two_node_cost + y_annotation ,text(('B':'Z')[j], 11))
    );
    @show spread_param[i].param
end


#==============================================================================#
#========  Optimal fixed-rate n parameter vs underlying inhomogeniety  ========# 
#==============================================================================#
scatter!(
    subplot = 6,
    range(0., stop=3, length=22),
    hcat(spread_param[paramfield]...)'[:,2],
    ms = 4,
    color=1,
    label= "Fixed-rate",
    primary=true,
    ylabel = "\$n\$-value", 
    xlabel = "\\delta", 
    ylims=(0.,11.),
    title="f)",
    legend=false,
    titleloc=:left,
    );

#==============================================================================#
#==================  Plot inhomogeneity vs n_data heatmaps  ===================# 
#==============================================================================#
    heatmap!(
        subplot=7,
        nrange,
        δrange,
        log10.(getfield.(pc_hetro_2, :cost)) |> permutedims;
        clims=(-2.,0),
        # colorbar_title = "Two-step cost [log10]",
        colorbar_title = L"C_{two-step}",
        ylabel = raw"$\delta$",
        lw=0,
        xlabel = raw"$n_{data}$",
        color = :viridis,
        extra_kwargs = Dict(:series=>Dict(
                                          "shader"=>"flat corner",
                                          "surf" => nothing,
                                         ),
                            :subplot => Dict(
                                             "colorbar style"=>raw"{at={(1.07, 1.0)},  ytick={-2, -1, 0}, title={$\log_{10}(Cost_{\textrm{\tiny two-step}})$}, xticklabel style={font={{\fontsize{8 pt}{10.4 pt}\selectfont}}, color={rgb,1:red,0.0;green,0.0;blue,0.0}, draw opacity={1.0}, rotate={0.0}}, yticklabel style={font={{\fontsize{8 pt}{10.4 pt}\selectfont}}, color={rgb,1:red,0.0;green,0.0;blue,0.0}, draw opacity={1.0}, rotate={0.0}}}",
                                            ),
                           ),
    );

    heatmap!(
        subplot=8,
        nrange,
        δrange,
        log10.(getfield.(pc_hetro_f, :cost)) |> permutedims;
        clims=(-2.,0),
        colorbar_title = "Fixed rate cost [log10]",
        ylabel = raw"$\delta$",
        xlabel = raw"$n_{data}$",
        color = :viridis,
        cbticks = [0 -1 -2],
        extra_kwargs = Dict(:series=>Dict(
                                          "shader"=>"flat corner",
                                          "surf" => nothing,
                                         ),
                            :subplot => Dict(
                                             "colorbar style"=>raw"{at={(1.07, 1.0)},  ytick={-2, -1, 0}, title={$\log_{10}(Cost_{\textrm{\tiny fixed-rate}})$}, xticklabel style={font={{\fontsize{8 pt}{10.4 pt}\selectfont}}, color={rgb,1:red,0.0;green,0.0;blue,0.0}, draw opacity={1.0}, rotate={0.0}}, yticklabel style={font={{\fontsize{8 pt}{10.4 pt}\selectfont}}, color={rgb,1:red,0.0;green,0.0;blue,0.0}, draw opacity={1.0}, rotate={0.0}}}",
                                             "at"=>"(-0.4, 1)",
                                            ),
                           ),
    );


    heatmap!(
        subplot=9,
        nrange,
        δrange,
        log10.(getfield.(pc_hetro_f, :cost) ./ getfield.(pc_hetro_2, :cost)) |> permutedims; 
        color=:balance,
        colorbar_title="Cost ratio [log10]",
        ylabel = raw"$\delta$",
        xlabel = raw"$n_{data}$",
        clims=(-2, 2),
        extra_kwargs = Dict(:series=>Dict(
                                          "shader"=>"flat corner",
                                          "surf" => nothing,
                                         ),
                            :subplot => Dict(
                                             "colorbar style"=>raw"{at={(1.07, 1.0)},  ytick={-2, -1, 0, 1, 2}, title={$\log_{10}(\frac{Cost_{\textrm{\tiny fixed-rate}}}{Cost_{\textrm{\tiny two-step}}})$}, xticklabel style={font={{\fontsize{8 pt}{10.4 pt}\selectfont}}, color={rgb,1:red,0.0;green,0.0;blue,0.0}, draw opacity={1.0}, rotate={0.0}}, yticklabel style={font={{\fontsize{8 pt}{10.4 pt}\selectfont}}, color={rgb,1:red,0.0;green,0.0;blue,0.0}, draw opacity={1.0}, rotate={0.0}}}",
                                            ),
                           ),
    );

#==============================================================================#
#=======================  Fine-tuning for publication  ========================# 
#==============================================================================#
for i in eachindex(plot!().subplots)
    plot!(subplot=i, title="$(('A':'Z')[i])", titleloc=(0.5, 0.75));
end

foreach(i -> plot!(subplot = i, titleloc=(0.5, 1.)), 7:9);
foreach(i -> plot!(subplot = i, left_margin = -2mm, right_margin=-2mm), 1:6);

map(subplot -> LinearNodes.reset_yticks!(plot!(), subplot), 2:5)

plot!(subplot=2, xlims=(0., 40), xticks=0:20:100, yticks=0:0.05:10);
plot!(subplot=3, xticks=0:40:80);
plot!(subplot=4, xticks=0:300:800, yticks=0:0.002:1);
plot!(subplot=5, xticks=0:3000:8000, yticks=0:0.0002:1);
plot!(subplot=6, yticks=0:5:10);
plot!(subplot=1, titlelocation=(0.5, 0.9));

plt = plot!()

s = plt.subplots[end-1]
     
# fieldnames(typeof(plt.backend))
savefig("tmp.tex")

#>>

#==============================================================================#
#==========================  Save the plot to file  ===========================# 
#==============================================================================#
plot!(dpi=300);
plot!(subplot=1:6, margin=-1mm);
plot!()


for format in [:png, :pdf, :svg]
    savefig("$root/results/inhomogeniety.$format")
end
