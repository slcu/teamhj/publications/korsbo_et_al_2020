using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using Plots
using FileIO
using JLD2
using LaTeXStrings
using Measures
import Unicode: uppercasefirst


pc = load(joinpath(root, "parameters", "ResultGeneral.jld2"), "pc")
pc = @filter(pc, input isa ImpulseInput)

## pgfplots was used to create the figure in the paper.
## However, with colouring of the dots, that takes ages, so I'm leaving gr as 
## the default here.
gr()


model_cost = :fixed_rate_cost
costfields = [
    :one_node_cost,
    :two_node_cost,
    :three_node_cost,
    :four_node_cost,
    :five_node_cost,
]

l = @layout [grid(1,4) a{0.23w}]
# plt = plot(layout = l, size = (800,130), fontfamily="Times");
plt = plot(layout = l, size = (800,180));
for (i, mamodel) in enumerate(costfields)
    scatter!(
        subplot = i,
        pc[mamodel],
        pc[model_cost],
        markerstrokealpha = 0.3,
        markersize = 2,
        markeralpha = 0.5,
        zcolor = length.(pc[:param]) .- 1,
        clims = (0, 50),
        colorbar_title = L"n_{data}",
        seriescolor = :viridis,
        cb = i == 5 ? true : false,
        bottom_margin = 5mm,
        left_margin = -2mm,
        right_margin = -2mm,
    );
    percentage_above = @eval round(Int, length(@filter(pc, $(model_cost) > $mamodel))/length(pc)*100)
    percent_sign = Plots.backend_name() == :pgfplotsx ? "\\%" : "%"
    annotation = [
        (0.3, 0.66, "$(percentage_above)$(percent_sign)"),
        (0.7, 0.33, "$(100-percentage_above)$(percent_sign)"),
    ]
    plot!(
        subplot = i,
        0:0.1:1,
        x->x,
        xlims = (0., 1.),
        ylims = (0., 1.),
        color = :grey,
        ylabel = i == 1 ? "Fixed rate cost" : "",
        xlabel = "$(uppercasefirst(split(string(mamodel), "_")[1]))-step cost",
        ticks = 3,
        annotate = annotation,
    );
end
plot!(legend = false, grid = false);

plot!(dpi = 600);

@time savefig(joinpath( root, "results", "truncated_vs_$(model_cost)_coloured_$(Plots.backend_name()).pdf",)) 

for format in [:pdf, :png] 
    savefig(joinpath(
        root,
        "results",
        "truncated_vs_$(model_cost)_coloured_$(Plots.backend_name()).$format",
    ))
end
