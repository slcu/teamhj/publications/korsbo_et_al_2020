
using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using FileIO
using Plots
pgfplotsx()

subplots = Dict(
                :cost => 1,
                :saturation => 2,
               )
plot(layout=(1,2), size=(600, 190));
##################### cost demo
target = simulate(FixedRateModel(), DecayingInput(), [1., 5., 0.8], tstop=17.)
model = simulate(FixedRateModel(), DecayingInput(), [1.0, 2., 0.3], tstop=17.)
plot!(subplot = subplots[:cost], 
    model,
    vars=2,
    lw=0,
    label = "Mismatch",
    fillrange = (x->target(x; idxs=2)),
    color = :grey,
    alpha = 0.4,
);
plot!(subplot = subplots[:cost], target, vars=2, lw=3, color=:black, label = "Data");
trange = range(0., stop=17, length = 100)
plot!(subplot = subplots[:cost], 
    model,
    vars=2,
    lw=3,
    color=1,
    label = "Model",
    # fillrange = (x->target(x; idxs=2)),
    fillcolor = :black,
    fillalpha = 0.2,
    legend=:topright,
    xlabel = "Time",
    ylabel = "Concentration",
    yticks=([-10.], [""]),
    xticks=([-10.], [""]),
    grid = false,
);
cost_value = LinearNodes.normalized_integral_cost(FixedRateModel(), DecayingInput(), [1., 2., 0.3], trange, target.(trange; idxs=2), 17.)
plot!(
      subplot=subplots[:cost],
      annotate= (6, 0.03, Text("Cost = $(round(cost_value, sigdigits=2))"), 10),
     );
##################### saturation demo
target = simulate(FixedRateCascadeModel(), PulseInput(), [1., 10., 2., 1.], tstop=30.)
int, ymax, t1, t2 = saturation_summary(FixedRateCascadeModel(), [1., 10., 2., 1.])
plot!(subplot = subplots[:saturation], [t1, t1, t2, t2, t1, t1], ymax .* [1/2, 1, 1, 1/2, 1/2, 1], lw = 2, color=:grey, label="");
# plot!(subplot = subplots[:saturation], [0., t1], ymax .* [1, 1], lw = 2, color=:grey, style=:dash);
# plot!(subplot = subplots[:saturation], [0., t1], ymax/2 .* [1, 1], lw = 2, color=:grey, style=:dash);
plot!(subplot = subplots[:saturation], 
    target,
    vars=2,
    lw=3,
    color=:black,
    label = "",
    # fillcolor = :black,
    # fillalpha = 0.2,
    legend=:topright,
    xlabel = "Time",
    ylabel = "Concentration",
);
plot!(subplot = subplots[:saturation], 
    range(t1, stop=t2, length=200),
    x->target(x; idxs=2),
    color=:black,
    label = "",
    fillrange = ymax/2,
    fillcolor = :black,
    fillalpha = 0.2,
);
plot!(subplot = subplots[:saturation], 
    yticks = ([ymax/2, ymax], ["\\frac{X_{max}}{2}", "X_{max}"]),
    xticks = (Float64[-2.], String[""]),
    grid = false,
    ticks = false,
    );
plot!(
      subplot=subplots[:saturation],
      annotate= (12.5, 0.07, Text("Saturation score = $(round(int, sigdigits=2))"), 10),
     );
plot!(dpi = 600);
plot!(title = permutedims(["$(('A':'Z')[i])" for i in eachindex(plot!().subplots)]));
plot!()
 
foreach(fmt -> savefig("results/cost_saturation_demo.$fmt"), [:png, :pdf])

