################################################################################
#
# Run the optimisation procedure in parallel.
#
# The main work is done by the a looped call to `p = opt(...)`, the rest is
# mainly to allow correct parallelism and file handling.
#
# To allow parallelism, call this script using
# julia -p N parallel_opt.jl
# Where N is the number of threads you want to allocate.
#
################################################################################

using Distributed

@everywhere using Pkg
@everywhere root_dir = joinpath(@__DIR__, "..")
println("Activating session in $(root_dir)")
@everywhere Pkg.activate(root_dir)
@everywhere using LinearNodes

using FileIO
using JLD2


param_file = joinpath(@__DIR__, "..", "parameters", "ResultGeneral.jld2")
isfile(param_file) ? (pc = load(param_file, "pc")) : (pc = ParamCollection([]))

# nr_previous = length(@filter(pc, length(param) <= 51))
# n = 2000 - nr_previous

n = 5000 # The number of optimisations to run for each input.
@show nprocs()

const jobs = RemoteChannel(()->Channel{AbstractInput}(nprocs()))

const results = RemoteChannel(()-> Channel{ResultGeneral}(nprocs()))

const inputs = [
                ImpulseInput(),
                StepInput(),
                NoiseInput(),
                PiecewiseInput(),
                WaveInput(),
                RampInput(),
               ]

@everywhere function run_jobs(jobs, results)
    while true
        input = take!(jobs)
        println("Assigning job with $(typeof(input)) to worker $(myid()).")
        flush(stdout)
        t1 = time()

        p = opt(input, max_nodes=50)

        println("This opt of $(typeof(input)) took $(round(time()-t1)) seconds.")

        put!(results, p)
    end
end


function make_jobs(n)
    for i in 1:n
        for input in inputs
            put!(jobs, input)
        end
    end
end

@async make_jobs(n)

for p in workers()
    remote_do(run_jobs, p, jobs, results)
end

@elapsed for i in 1:length(inputs)*n
    p = take!(results)

    push!(pc, p)
    save(param_file, "pc", pc)


    jldopen(replace(param_file, ".jld2" => "_backup.jld2"), true, true, true, IOStream) do file
        write(file, "pc", pc)
    end

    println("Done saving param nr $(i) of $(length(inputs)*n).")
    flush(stdout)
end

println("Finished with all specified optimisations!")
