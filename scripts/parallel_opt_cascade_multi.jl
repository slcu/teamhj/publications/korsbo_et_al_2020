
################################################################################
#
# Run the optimisation procedure in parallel.
#
# The main work is done by the a looped call to `p = opt(...)`, the rest is
# mainly to allow correct parallelism and file handling.
#
# To allow parallelism, call this script using
# julia -p N parallel_opt.jl
# Where N is the number of threads you want to allocate.
#
################################################################################

using Distributed

@everywhere using Pkg
@everywhere root_dir = joinpath(@__DIR__, "..")
println("Activating session in $(root_dir)")
@everywhere Pkg.activate(root_dir)
@everywhere using LinearNodes
@everywhere using DifferentialEquations

using FileIO
using JLD2


param_file = joinpath(@__DIR__, "..", "parameters", "ResultMultiCascadeMultiTargetRedefined.jld2")
isfile(param_file) ? (pc = load(param_file, "pc")) : (pc = ParamCollection([]))

n = 5 - length(pc) # The number of optimisations 
@show nprocs()

const jobs = RemoteChannel(()->Channel{Int}(nprocs()))

const results = RemoteChannel(()-> Channel{ResultMulti}(nprocs()))

@everywhere function run_jobs(jobs, results)
    while true
        job_nr = take!(jobs)
        println("Assigning job to worker $(myid()).")
        flush(stdout)
        t1 = time()

        p = opt_multi(CascadeModel(), [:cascade_2, :fixed_rate_cascade]; tstop=400., alg=Rosenbrock23(), silent=true, inputs=[PulseInput(10. .^ γ, 1.) for γ in -3:3:3])

        println("This opt took $(round(time()-t1)) seconds.")

        put!(results, p)
    end
end


function make_jobs(n)
    for i in 1:n
        put!(jobs, i)
    end
end

@async make_jobs(n)

for p in workers()
    remote_do(run_jobs, p, jobs, results)
end

@elapsed for i in 1:n
    p = take!(results)

    push!(pc, p)
    save(param_file, "pc", pc)


    jldopen(replace(param_file, ".jld2" => "_backup.jld2"), true, true, true, IOStream) do file
        write(file, "pc", pc)
    end

    println("Done saving param nr $i of $n.")
    flush(stdout)
end

println("Finished with all specified optimisations!")
