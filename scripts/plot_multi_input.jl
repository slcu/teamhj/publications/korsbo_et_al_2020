using Pkg
path = string(split(@__DIR__, "scripts")[1])
Pkg.activate(path)
using LinearNodes
using FileIO
using JLD2
using Plots

pgfplotsx()


function move_annotation!(plt, subplot, upper, x, y)
    ann = plt.subplots[subplot].attr[:annotations]
    idx = upper ? 2 : 1
    ann[idx] =(ann[idx][1] + x, ann[idx][2] + y, ann[idx][3]) 
    plot!(plt, subplot=subplot, ann=ann)
end

pc = load("$path/parameters/ResultGeneral.jld2", "pc")


plt = multiinputplot(
    pc;
    model=:two_node, 
    comparison=:fixed_rate, 
    cost_scale=:log10, 
    markeralpha=0.01,
    linestyle=:solid,
    size = (700, 500),
    title = permutedims(["$fig" for fig in 'A':'Z']),
    aspect_ratio = :none,
    plotcolors = Dict(:fixed_rate=>1, :two_node=>2, :dde=>3, :data=>:black, :input=>:grey),
    title_location = (0.5, 0.9),
    extra_kwargs=Dict(:subplot=>Dict("legend columns"=>"-1", "legend style"=>"{at={(-0.3, 1.6)}, /tikz/every even column/.append style={column sep=0.5cm}}")),
);
plot!(subplot=4, legend=true);
map(subplot -> LinearNodes.reset_yticks!(plt, subplot), 2:2:12);
move_annotation!(plt, 7, false, 0, -1);
move_annotation!(plt, 9, false, -0.9, -0.5);
move_annotation!(plt, 11, false, -0.9, -0.5);


for format in [:png, :pdf]
    savefig("$path/results/multi_input.$format")
end
