using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

isdir(joinpath(root, "results")) || mkdir(joinpath(root, "results"))

using LinearNodes
using Plots
using FileIO
using JLD2
using Measures
using Latexify
path = split(@__DIR__, "scripts")[1]

#==============================================================================#
#==========  Define parameters for the plotting and load datasets.  ===========# 
#==============================================================================#
pgfplotsx()
plot(x->x)

pc = load("$path/parameters/ResultGeneral.jld2", "pc")
pc_homogeneous = load("parameters/ResultGeneralHomogeneous.jld2", "pc")

p_noise = @filter pc input isa AbstractNoiseInput
p_step = @filter pc input isa StepInput
p_piecewise = @filter pc input isa PiecewiseInput
p_wave = @filter pc input isa WaveInput
p_impulse = @filter pc input isa ImpulseInput
p_ramp = @filter(pc, input isa RampInput && two_node_cost < 10)


#==============================================================================#
#=====  Plot for a diverse range of inputs (not included in the paper).  ======# 
#==============================================================================#

for params in [p_noise, p_step, p_piecewise, p_wave, p_ramp]
    plt = costdisplay(
        params,
        :two_node,
        ylims_cost=(-0.2, 1.15),
        dpi=300,
    );
    map(subplt -> LinearNodes.reset_yticks!(plt, subplt), vcat(1:5, 7:11));
    #
    input_string = split("$(typeof(params[1].input))", "Input")[1]
    for format in [:png, :pdf]
        savefig("$path/results/$(input_string)_two_node_display_$(Plots.backend_name()).$format")
    end
    #
    plt = costdisplay(
        params,
        :fixed_rate; 
        comparison=:two_node,
        ylims_cost=(-0.2, 1.15),
        dpi=300,
    );
    map(subplt -> LinearNodes.reset_yticks!(plt, subplt), vcat(1:5, 7:11));
    #
    for format in [:png, :pdf]
        savefig("$path/results/$(input_string)_fixed_rate_display_$(Plots.backend_name()).$format")
    end
end

#==============================================================================#
#==========  Plot with impulse input (main figures of the paper).  ===========# 
#==============================================================================#

max_nodes = 50


#==============================================================================#
#=============================  Fixed-step plots  =============================# 
#==============================================================================#

for nodes in [:one_node, :two_node, :three_node, :four_node, :five_node]
    plt = costdisplay(
        p_impulse,
        nodes; 
        homogeneous_param=pc_homogeneous,
        dpi=600,
        max_nodes=max_nodes,
        ylims_cost=(-0.2, 1.19),
        tstop_factor=2/3,
        left_margin=-2mm,
        margin=-1mm,
        titleloc = (0.5, 0.74),
    );
    plot!(subplot=6, titleloc=(0.5, 0.88));
    plot!(subplot=5, right_margin=0mm);
    plot!(subplot=11, right_margin=0mm);
    map(subplt -> LinearNodes.reset_yticks!(plt, subplt), vcat(1:5, 7:11));
    if nodes == :two_node
        plot!(subplot=8, xticks=0:150:1000);
    elseif nodes == :one_node
        plot!(subplot=8, xticks=0:150:1000);
    end;
    for format in [:png, :pdf]
        savefig("$path/results/impulse_$(nodes)_display_$(max_nodes)_nodes_$(Plots.backend_name()).$format")
    end
end

#==============================================================================#
#=============================  Fixed-rate plot  ==============================# 
#==============================================================================#

plt = costdisplay(
    p_impulse,
    :fixed_rate; 
    comparison=:two_node,
    dpi=300,
    ylims_cost=(-0.2, 1.15),
    tstop_factor=3/4,
    max_nodes=max_nodes,
    k_max=3,
    k_ideal=3,
    k_min=3,
    left_margin=-2mm,
    margin=-1mm,
    titleloc = (0.5, 0.75),
    annotation_y_shifts = [0, 0.22, 0.45, 0, 0],
);
plot!(subplot=6, titleloc=(0.5, 0.88));

# Fine-tune ticks for publication. 
map(subplt -> LinearNodes.reset_yticks!(plt, subplt), vcat(1:5, 7:11));
if max_nodes == 50
    plot!(subplot=7, xlims=(0.,2), xticks=0:1:2);
    plot!(subplot=4, xlims=(0.,600), xticks=0:300:5000);
    plot!(subplot=5, xlims=(0.,800), xticks=0:400:5000);
    plot!(subplot=11, xticks=0:1000:5000);
end;

fixed_rate_plot = deepcopy(plt);

#==============================================================================#
#==========  Parameter analysis, subfigures l, m, n, o of Figure 5  ===========# 
#==============================================================================#


# model_param = :gamma_param
model_param = :fixed_rate_param
model_cost = :fixed_rate_cost

parameter_plot = plot(layout=grid(1,4));


######################################################
#  n_model vs n_data
######################################################
n_subplot = 2
scatter!(
    subplot=n_subplot, 
    [length(p.param) for p in p_impulse], 
    [getfield(p, model_param)[2] for p in p_impulse],
    alpha=0.3,
    color = 1,
    label="",
);
plot!(subplot=n_subplot, x->x, 0:50,  label="", color=:grey);
plot!(subplot=n_subplot,  ylabel=latexify("n"), ylims=(0., Inf));
plot!(subplot=n_subplot, xlabel="\$n_{data}\$");

######################################################
#  r vs n_data
######################################################

r_subplot = 3
scatter!( subplot=r_subplot,
    [minimum(p.param[2:end]) for p in p_impulse],
    [getfield(p, model_param)[3] for p in p_impulse],
    alpha=0.3,
    scale=:log10,
    color = 1,
    label="",
    xlims=(1e-2, 1e1),
    ylims=(1e-2, 1e1),
);
plot!(subplot = r_subplot, x->x, 10. .^(-2:0.1:1), label="", color=:grey);
using LaTeXStrings
plot!(subplot = r_subplot, ylabel=latexify("r"), xlabel=L"min(r_{data})", xrotation=3.14);

######################################################
#  γ vs n_data
######################################################
gamma_subplot=  1
scatter!( subplot=gamma_subplot,
    length.(p_impulse[:param]),
    [p[1] for p in p_impulse],
    alpha=0.3,
    color = 1,
    label="",
);
plot!(subplot = gamma_subplot, ylabel=L"\gamma", ylims=(0.,2.), xlabel="\$n_{data}\$");


############################################################
#  cost comparison, Gamma model vs Mass action 
############################################################

cost_subplot = 4
scatter!(
    subplot= cost_subplot, 
    p_impulse[:two_node_cost],
    p_impulse[model_cost],
#         marker_z = length.(p_impulse[:param]) .- 1,
#         color=:Blues,
#         clims=[1, 25]
#         zcolor = [colormap("Blues",25)[i-1] for i in length.(p_impulse[:param])]',
    alpha = 0.3,
);
percentage_above = @eval round(Int, length(@filter(p_impulse, $(model_cost) > two_node_cost))/length(p_impulse)*100)
plot!(subplot=cost_subplot, annotate=[(0.3, 0.66, "$(percentage_above)\\%")]);
plot!(subplot=cost_subplot, annotate=[(0.7, 0.33, "$(100-percentage_above)\\%")]);
plot!(subplot=cost_subplot, 0:0.1:1, x->x, xlims=(0., 1.), ylims=(0., 1.), color=:grey);
plot!(subplot=cost_subplot, ylabel="Fixed rate cost", xlabel="Two-step cost");
plot!(subplot=cost_subplot, legend=false);



############################################################
#  Plot formatting
############################################################

for (i, title) in enumerate(["L", "M", "N", "O"])
    plot!(subplot = i, title=title, titleloc=(0.5, 0.75));
end
plot!(size=(600,130), grid=false, margins=-4mm, left_margin=-5mm);

#==============================================================================#
#====================  Save the fixed-rate figure - Fig 5  ====================# 
#==============================================================================#

l = @layout [a{0.75h}; b]
plt = plot(fixed_rate_plot, parameter_plot, layout=l, size=(600, 500))


for format in [:png, :pdf]
    savefig("$path/results/impulse_fixed_rate_$(max_nodes)_nodes_$(Plots.backend_name()).$format")
end


#==============================================================================#
#=============================  Gamma model plot  =============================# 
#==============================================================================#
using Measures

plt = costdisplay(
    p_impulse,
    :gamma; 
    comparison=:two_node,
    dpi=600,
    max_nodes=max_nodes,
    ylims_cost=(-0.2, 1.15),
    tstop_factor=3/4,
    k_max=3,
    k_ideal=3,
    k_min=3,
    left_margin=-1mm,
    right_margin=-1mm,
    top_margin=-1mm,
    title_location = (0.5, 0.75),
    annotation_y_shifts = [0, 0.22, 0.45, 0, 0],
);
foreach(i -> plot!(subplot=i, ylabel=""), vcat(2:5, 8:11));

# Fine-tune ticks for publication. 
map(subplt -> LinearNodes.reset_yticks!(plt, subplt), vcat(1:5, 7:11));
if max_nodes == 50
    plot!(subplot=7, xlims=(0.,2), xticks=0:1:2)
    plot!(subplot=4, xlims=(0.,600), xticks=0:300:5000)
    plot!(subplot=5, xlims=(0.,800), xticks=0:400:5000)
    plot!(subplot=9, xlims=(0.,600), xticks=0:300:5000)
    plot!(subplot=11, xticks=0:1000:5000)
end;

plot!()

for format in [:png, :pdf]
    savefig("$path/results/impulse_gamma_display_$(max_nodes)_nodes_$(Plots.backend_name()).$format")
end

