
################################################################################
#
# Run the optimisation procedure in parallel.
#
# The main work is done by the a looped call to `p = opt(...)`, the rest is
# mainly to allow correct parallelism and file handling.
#
# To allow parallelism, call this script using
# julia -p N parallel_opt.jl
# Where N is the number of threads you want to allocate.
#
################################################################################

using Distributed

@everywhere using Pkg
@everywhere root_dir = joinpath(@__DIR__, "..")
println("Activating session in $(root_dir)")
@everywhere Pkg.activate(root_dir)
@everywhere using LinearNodes
using ProgressMeter

@everywhere using FileIO
@everywhere using JLD2

# param_file = joinpath(@__DIR__, "..", "parameters", "ResultCascadeMultiTargetPulseForceDtmin.jld2")
@everywhere param_file = joinpath(@__DIR__, "..", "parameters", "ResultCascadeSingleTargetPulseForceDtmin.jld2")
save_file = joinpath(@__DIR__, "..", "parameters", "ResultCascadeSingleTargetPulseForceDtminReoptWorst.jld2")
@everywhere pc = load(param_file, "pc")

# sat = @showprogress [saturation_integral(p; force_dtmin=true, alg=Rosenbrock23()) for p in pcs]
sat = load("../parameters/SaturationScoresResultCascadeSingleTargetPulseForceDtmin.jld2", "sat")

function get_bad_idxs(pc, saturation_vector, model_key::Symbol; nbins=30, fraction=0.1)
    idxs_sorted = sortperm(saturation_vector)
    idxs_bad = []
    for i in 1:nbins
        idxs_in_bin = idxs_sorted[round(Int, (i-1)*end/nbins)+1:round(Int, i*end/nbins)]
        worst_idxs = idxs_in_bin[sortperm(cost.(pc[idxs_in_bin], model_key); rev=true)[1:round(Int, end*fraction)]]
        append!(idxs_bad, worst_idxs)
    end
    return idxs_bad
end

worst_idxs = Dict()
foreach(key -> worst_idxs[key] = get_bad_idxs(pc, sat, key), [:cascade_1, :cascade_2, :fixed_rate_cascade])



n = length(worst_idxs[:cascade_2]) + length(worst_idxs[:fixed_rate_cascade]) # The number of optimisations 
@show nprocs()

const jobs = RemoteChannel(()->Channel{Tuple{Result, Int, Symbol}}(nprocs()))

const results = RemoteChannel(()-> Channel{Tuple{Result, Int}}(nprocs()))

@everywhere function run_jobs(jobs, results)
    while true
        (p, idx, model_key) = take!(jobs)
        println("Assigning job to worker $(myid()).")
        flush(stdout)
        t1 = time()

        p_new = optimise(p; model_keys=[model_key], overwrite=true)

        println("This opt took $(round(time()-t1)) seconds.")

        put!(results, (p_new, idx))
    end
end


function make_jobs(n)
    for model_key in [:cascade_2, :fixed_rate_cascade]
        for i in worst_idxs[model_key]
            put!(jobs, (pc[i], i, model_key))
        end
    end
end

@async make_jobs(n)

for p in workers()
    remote_do(run_jobs, p, jobs, results)
end

@elapsed for i in 1:n
    p, j = take!(results)

    # push!(pc, p)
    pc[j] = p
    save(save_file, "pc", pc)


    jldopen(replace(save_file, ".jld2" => "_backup.jld2"), true, true, true, IOStream) do file
        write(file, "pc", pc)
    end

    println("Done saving param nr $i of $n.")
    flush(stdout)
end

println("Finished with all specified optimisations!")
