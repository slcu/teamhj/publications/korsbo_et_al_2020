using Pkg
root = string(split(@__DIR__, "scripts")[1])
Pkg.activate(root)

using LinearNodes
using Plots
using FileIO
using JLD2
using LaTeXStrings
using Statistics

pgfplotsx()

pc = load("parameters/ResultGeneral.jld2", "pc")
pc = @filter pc input isa ImpulseInput


plot_rows = 3
plot_cols = 4
plot(layout=(plot_rows, plot_cols), size=(800, 500), legend=:topleft, grid=false);
plot!(subplot=1, ylabel=L"\textrm{Sharpness, }\langle t \rangle / \sigma_t");
plot!(xlabel=L"n_{data}");
# plot();
for n in 2:5
    i = n - 1
    sharpnessplot!(subplot = i, pc, plotmodels=[:data, n])
    i != 1 && ylabel!(subplot=i, "")
end
plot!(subplot=1 + plot_cols, ylabel=L"\sigma_r/\langle r \rangle");
plot!(xlabel=L"n_{data}");
for n in 2:5
    i = n - 1 + plot_cols
    scatter!(
        subplot=i,
        length.(pc[:param]) .- 1,
        rate_homogeneity.(pc),
        scale=:identity,
        label = "Data",
        color = :black,
        alpha = 0.3,
    );
    scatter!(
        subplot=i,
        length.(pc[:param]) .- 1,
        rate_homogeneity.(pc, n),
        scale=:identity,
        color = n,
        label = "$n-step",
        alpha = 0.3,
    );
    # plot!(subplot=n, x->x^2/n; label= "\$f(x) = x^2/$n\$", color = n);
end
pc_filtered = @filter(pc, length(param) == 26)
median_idx = sortperm(t_mean.(pc_filtered).^2 ./ t_var.(pc_filtered))[round(Int, end/2)]
p = pc_filtered[median_idx]
for n in 2:5
    i = n - 1 + 2*plot_cols
    j = n - 1
    scatter!(
        subplot=j + 0 * plot_cols,
        fill(length(p) - 1, 2),
        [sharpness(p), sharpness(p, n)],
        label = "",
        shape=:diamond,
        color = :white,
        ms = 9,
    );
    scatter!(
        subplot=j + 1 * plot_cols,
        fill(length(p) - 1, 2),
        [rate_homogeneity(p), rate_homogeneity(p, n)],
        label = "",
        shape=:diamond,
        color = :white,
        ms = 9,
        legend=false,
    );
    # plot!(subplot=n, x->x^2/n; label= "\$f(x) = x^2/$n\$", color = n);
    plot!(
        subplot=i,
        p,
        plotmodels=[:data, n],
        color = [:black n],
        legend=false,
    )
end
foreach(i->plot!(subplot=i; title="$(('a':'z')[i]))", title_location=:left), eachindex(plot!().subplots))
plt = plot!()

savefig("results/peak_shape_bounds.pdf")
savefig("results/peak_shape_bounds.svg")
savefig("results/peak_shape_bounds.png")

pgfplotsx()


pc_filtered = @filter(pc, length(param) == 26)
median_idx = sortperm(t_mean.(pc_filtered) .^ 2 ./ t_var.(pc_filtered))[round(Int, end / 2)]
p = pc_filtered[median_idx]
#
plot(grid = false);
scatter!(sharpness.(pc), rate_homogeneity.(pc), alpha = 0.1, color = :grey, label = "Data");
for i in [5, 2]
    pc_longer = @filter(pc, length(param) - 1 >= i)
    scatter!(
        sharpness.(pc_longer),
        rate_homogeneity.(pc_longer, i),
        color = i,
        alpha = 0.3,
        label = "$i-step model",
    )
end
xticks!(
    [1:4; [sqrt(i) for i in [2, 5]]],
    LaTeXString.(string.([1:4; ["\$\$\\sqrt{$i}\$\$" for i in [2, 5]]])),
);
plot!(
    xlabel = L"\textrm{Data sharpness, }s_{data}",
    ylabel = L"\textrm{Rate dispersion, }\sigma_r/\langle r \rangle",
);
plt2 = plot!()


pc_filtered = @filter(pc, length(param) == 26)
median_idx = sortperm(t_mean.(pc_filtered) .^ 2 ./ t_var.(pc_filtered))[round(Int, end / 2)]
p = pc_filtered[median_idx]
#
l = @layout(grid(2, 2))
plot(layout = l; grid = false, size = (600, 300));
#=============================  Sharpness plots  ==============================#
sharpnessplot!(
    subplot = 1,
    pc,
    plotmodels = [:data, 5, 2],
    legend = :topleft,
    p_highlight = [p],
    yticks = (  
              vcat(0:6, sqrt(5), sqrt(2)),
              # LaTeXString.(string.([0:6; L"$$\sqrt{5}$$"]))
              LaTeXString.(vcat([i % 3 == 0 ? string(i) : "" for i in  0:6], L"$$\sqrt{5}$$", L"$$\sqrt{2}$$")) 
             ),
    markerstrokealpha = [0.1 0.1 0.1 1 1 1],
);
# sharpnessplot!(
#     subplot = 1,
#     pc,
#     plotmodels = [:data, 2],
#     legend = :topleft,
#     p_highlight = [p],
#     yticks = (  
#               [0:6; sqrt(2)],
#               LaTeXString.([[i % 3 == 0 ? string(i) : "" for i in  0:6]; L"$$\sqrt{2}$$"])
#              ),
#     yminorticks= 2,
# );
#======================  rate homogeneity vs sharpness  =======================#
subplot = 2
scatter!(
    subplot = subplot,
    sharpness.(pc),
    rate_homogeneity.(pc),
    alpha = 0.1,
    markercolor = :black,
    label = "Data",
    markerstrokealpha = 0.1,
);
for i in [5, 2]
    pc_longer = @filter(pc, length(param) - 1 >= i)
    scatter!(
        subplot = subplot,
        sharpness.(pc_longer),
        rate_homogeneity.(pc_longer, i),
        markercolor = i,
        alpha = 0.3,
        label = "$i-step model",
        markerstrokealpha = 0.1,
    )
    scatter!(
        subplot = subplot,
        [sharpness(p)],
        [rate_homogeneity(p, i) rate_homogeneity(p)],
        color = :white,
        label = "",
        shape = :diamond,
        ms = 6,
    )
end
xticks!(
    subplot = subplot,
    [1:4; [sqrt(i) for i in [2, 5]]],
    LaTeXString.(string.([1:4; ["\$\$\\sqrt{$i}\$\$" for i in [2, 5]]])),
);
plot!(
    subplot = subplot,
    xlabel = L"\textrm{Data sharpness, }s_{data}",
    # ylabel = L"\textrm{Rate dispersion, }\sigma_r/\langle r \rangle",
    ylabel = "Rate dispersion",
    legend = :topright,
    ylims = (0., 3.5),
    yticks = 0:3,
);
#======================  rate homogeneity vs n_data  =======================#
subplot = 3
scatter!(
    subplot = subplot,
    length.(pc) .- 1,
    rate_homogeneity.(pc),
    alpha = 0.1,
    markercolor = :black,
    label = "Data",
    markerstrokealpha = 0.1,
);
for i in [5, 2]
    pc_longer = @filter(pc, length(param) - 1 >= i)
    scatter!(
        subplot = subplot,
        length.(pc_longer) .- 1,
        rate_homogeneity.(pc_longer, i),
        markercolor = i,
        alpha = 0.3,
        label = "$i-step model",
        markerstrokealpha = 0.1,
    )
    scatter!(
        subplot = subplot,
        [length(p) - 1],
        [rate_homogeneity(p, i) rate_homogeneity(p)],
        color = :white,
        label = "",
        shape = :diamond,
        ms = 6,
    )
end
plot!(
    subplot = subplot,
    xlabel = L"n_{data}",
    # ylabel = L"\textrm{Rate dispersion, }\sigma_r/\langle r \rangle",
    ylabel = "Rate dispersion",
    legend = :topright,
    ylims = (0., 3.5),
    yticks = 0:3,
);
#=============================  Trajectory demo  ==============================#
plot!(subplot = 4, p; plotmodels = [:data, 5, 2], legend = :topright, label=["Data" "Five-step model" "Two-step model"]);
foreach(
    i -> plot!(subplot = i; title = "$(('a':'z')[i]))", title_location = :left),
    eachindex(plot!().subplots),
)
plot!(subplot=1; legend=false);
plot!(subplot=2; legend=false);
plot!(subplot=3; legend=false);
plot!()


plot!(dpi=600);
plotname = "fixed_step_sharpness_analysis"
foreach(fmt -> savefig("results/$plotname.$fmt"), ["png", "pdf", "svg"])
plot!(dpi=90);



# plot(layout = (1,2), grid=false, size = (600, 150));
plot(layout = (1,2), grid=false, size = (477.7, 130));
plot!(guidefontsize=10, tickfontsize=7, legendfontsize=10);
#=============================  Fast extra rates  =============================#
subplot = 2
pc_filtered = @filter(pc, length(param) == 3)
x = 1:50
y = hcat([sort(p.three_node_param[2:end]) for p in pc_filtered][x]...)
y_data = hcat([sort(p.param[2:end]) for p in pc_filtered][x]...)
scatter!(
    subplot = subplot,
    x,
    y',
    yscale = :log10,
    label = [L"r_{slow}" L"r_{fast}" L"r_{extra}"],
);
scatter!(
    subplot = subplot,
    x,
    y_data',
    yscale = :log10,
    markershape = :cross,
    # label = ["True values" ""],
    label = "",
    ms = 6,
);
plot!(subplot = subplot, legend = :topright, xlabel = "Parameter set", ylabel = "Parameter value");
#==========================  Order does not matter  ===========================#
p = @filter(pc, length(param) == 10)[5]
tstop = 200
order_subplot = 1
plot!(
    subplot = order_subplot,
    simulate(model(p), p.input, p, tstop = tstop),
    vars = 2,
    lw = 5,
    label = "Original",
);
plot!(
    subplot = order_subplot,
    simulate(model(p), p.input, [p[1]; sort(p[2:end])], tstop = tstop),
    vars = 2,
    style = :dash,
    lw = 5,
    label = "Sorted",
);
plot!(
    subplot = order_subplot,
    xlims = (0.0, tstop),
    ylims = (0.0, 0.0125),
    yticks = 0:0.005:0.01,
    ylabel = "Concentration",
    xlabel = "Time",
    legend = :topright,
    grid = false,
);
foreach(
    i -> plot!(subplot = i; title = "$(('a':'z')[i]))", title_location = :left),
    eachindex(plot!().subplots),
)
plot!()

plot!(dpi=600);
plotname = "fixed_step_rate_order"
foreach(fmt -> savefig("results/$plotname.$fmt"), ["png", "pdf", "svg"])
plot!(dpi=90);
