using Dates

using Pkg
root = split(@__DIR__(), "scripts")[1]
cd(root)
Pkg.activate(".")
script_path = joinpath(root, "scripts")
isdir(joinpath(root, "results")) || mkdir(joinpath(root, "results"))


# scripts = [f for f in readdir(script_path) if startswith(f, "plot_")]
scripts = [
    "plot_biology_examples.jl",
    "plot_cascade_fit.jl",
    "plot_cost_demo.jl",
    "plot_cost_vs_nonlinearity.jl",
    "plot_dde.jl",
    "plot_display.jl",
    "plot_gamma_comparison.jl",
    "plot_inhomogeniety_analysis.jl",
    "plot_multi_input.jl",
    "plot_param_analysis_fixed_step.jl",
    "plot_sharpness.jl",
    "plot_truncated_cost.jl",
]

println("The following $(length(scripts)) scripts will be run:")
map(println, scripts);

for script in scripts
    println("$(now()) - Running script: $script")
    include(joinpath(script_path, script));
end

println("$(now()) - Finished!")
println("You should now have a lot of plots in the 'results' directory.")
